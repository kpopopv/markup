<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            'cookieValidationKey' => 'muEgLKg9Kt9iFDCsSj-VJgAcAKgTu_1w',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),

        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                '/ui' => 'site/ui',
                '/page1' => 'site/page1',
                '/page2' => 'site/page2',
                '/page3' => 'site/page3',
                '/page4' => 'site/page4',
                '/page5' => 'site/page5',
                '/signup' => 'site/signup',
                '/signin' => 'site/signin',
                '/forgotPassword' => 'site/forgot-password',
                '/app' => 'site/app',
                '/appReg' => 'site/app-reg',
                '/appView' => 'site/app-view',
                '/dashboard' => 'site/user-dashboard',
                '/dashboardProject' => 'site/user-dashboard-project',
                '/project1' => 'site/project1',
                '/project2' => 'site/project2',
                '/project3' => 'site/project3',
                '/project4' => 'site/project4',
                '/project5' => 'site/project5',
                '/project6' => 'site/project6',
                '/account' => 'site/account',
                '/account1' => 'site/account1',
                '/account2' => 'site/account2',
                '/account3' => 'site/account3'
            ]
        ],
        'view' => [
            'theme' => [
                'pathMap' => ['@app/views' => '@app/themes/leantesting/views'],
                'baseUrl' => '@web/themes/leantesting',
            ],
        ]
    ],
    'params' => $params,
];

//if (YII_ENV_DEV) {
//    // configuration adjustments for 'dev' environment
//    $config['bootstrap'][] = 'debug';
//    $config['modules']['debug'] = [
//        'class' => 'yii\debug\Module',
//    ];
//
//    $config['bootstrap'][] = 'gii';
//    $config['modules']['gii'] = [
//        'class' => 'yii\gii\Module',
//    ];
//}

return $config;
