<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;


class SiteController extends Controller
{
    public function behaviors()
    {
        return [
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionUi()
    {
        return $this->render('ui');
    }

    public function actionPage1()
    {
        return $this->render('page1');
    }

    public function actionPage2()
    {
        return $this->render('page2');
    }

    public function actionPage3()
    {
        return $this->render('page3');
    }

    public function actionPage4()
    {
        return $this->render('page4');
    }

    public function actionPage5()
    {
        return $this->render('page5');
    }
    
    public function actionSignup(){
        $this->layout = 'dialogs';
        return $this->render('sign-up');
    }

    public function actionSignin(){
        $this->layout = 'dialogs';
        return $this->render('sign-in');
    }

    public function actionForgotPassword(){
        $this->layout = 'dialogs';
        return $this->render('forgot-password');
    }

    public function actionApp(){
        $this->view->params['body_layout'] = 'apps-layout';
        return $this->render('app');
    }

    public function actionAppReg(){
        $this->view->params['body_layout'] = 'apps-layout';
        return $this->render('app-reg');
    }

    public function actionAppView(){
        $this->view->params['body_layout'] = 'apps-layout';
        return $this->render('app-view');
    }
    
    public function actionUserDashboard(){
        $this->view->params['body_layout'] = 'dashboard-layout';
        return $this->render('dashboard');
    }

    public function actionUserDashboardProject(){
        $this->view->params['body_layout'] = 'dashboard-layout';
        return $this->render('dashboard-project');
    }

    public function actionProject1(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project1');
    }

    public function actionProject2(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project2');
    }

    public function actionProject3(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project3');
    }

    public function actionProject4(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project4');
    }

    public function actionProject5(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project5');
    }

    public function actionProject6(){
        $this->view->params['body_layout'] = 'project-layout';
        return $this->render('project6');
    }
    
    public function actionAccount(){
        $this->view->params['body_layout'] = 'account-layout';
        return $this->render('account');
    }

    public function actionAccount1(){
        $this->view->params['body_layout'] = 'account-layout';
        return $this->render('account1');
    }

    public function actionAccount2(){
        $this->view->params['body_layout'] = 'account-layout';
        return $this->render('account2');
    }

    public function actionAccount3(){
        $this->view->params['body_layout'] = 'account-layout';
        return $this->render('account3');
    }

}
