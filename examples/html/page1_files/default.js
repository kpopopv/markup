(function($) {

    "use strict";

    function dropdownShow(t) {
        var self, btEl, aEls;
        self = $(t.target);
        btEl = self.find(".btn");
        aEls = self.find("a");

        if (btEl.length === 0 || aEls.length === 0) { return; }

        btEl.data("toggleText", btEl.html());
        btEl.data("toggleClass", btEl.attr("class"));

        btEl.html("Select");
        btEl.attr("class", "btn no-select btn-fix-w-114 btn-primary-light_white dropdown-toggle");

        aEls.each(function() {
            var clsName;
            clsName = "";
            if ($(this).data("toggleText") === btEl.data("toggleText")) { clsName = "active"; }

            this.parentNode.className = clsName;
        });
    }
    function dropdownHide(t) {
        var self, btEl;
        self = $(t.target);
        btEl = self.find(".btn");

        if (btEl.length === 0) { return; }

        btEl.html(btEl.data("toggleText"));
        btEl.attr("class", btEl.data("toggleClass"));
    }
    function dropdownSelect(t) {
        var self, rootEl, btEl;
        self = $(t.target);
        rootEl = $(t.target.parentNode.parentNode.parentNode);

        btEl = rootEl.find(".btn");

        if (btEl.length === 0 ) { return; }
        if (self[0].tagName != "A") { self = self.find("a"); }

        btEl.data("toggleText", self.data("toggleText"));
        btEl.data("toggleClass", ("dropdown-toggle no-select btn btn-fix-w-114 btn-primary-"+self.data("toggleClass")));
    }

    $(document).ready(function() {
        var dropdownToggle;
        dropdownToggle = $(".dropdown-toggle").parent();

        dropdownToggle.on("show.bs.dropdown", dropdownShow);
        dropdownToggle.on("hide.bs.dropdown", dropdownHide);
        dropdownToggle.find("li").on("click", dropdownSelect);

    });

})(jQuery);
