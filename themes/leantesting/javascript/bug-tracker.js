(function($) {

    var bugTrackerBar, menu, btnShowBugTracker;

    function toggleMenu(){
        if(menu.hasClass('initialized')) menu.removeClass('initialized');
        menu.toggleClass('collapsed');
    }

    function expandContent(){
        $('.content').parent().removeClass('col-lg-8').addClass('col-lg-9 col-lg-offset-2');
    }

    function reduceContent(){
        $('.content').parent().addClass('col-lg-8').removeClass('col-lg-9 col-lg-offset-2');
    }

    function init(){
        menu = $('.left-navbar-nav');
        if(typeof(page_id) != 'undefined' && page_id == 'project6'){
            bugTrackerBar = $('.bug-tracker-bar');
            btnShowBugTracker = $('#btn-show-bug-tracker');
            return true;
        }
        return false;
    }

    $(document).ready(function() {
        // If is not bug track page
        if(!init()){
            $("#btn-collapse-navbar").click(function(){
                $(this).toggleClass('active');
                
                toggleMenu();
            });
            return false;
        }

        $("#btn-collapse-navbar").click(function(){
            $(this).toggleClass('active');

            if (!bugTrackerBar.hasClass("hidden") && (!menu.hasClass('collapsed') || menu.hasClass('initialized'))) {
                bugTrackerBar.animate({left:$('.bug-tracker-bar').width() * -1},function(){
                    btnShowBugTracker.show();
                    bugTrackerBar.addClass('hidden');
                    toggleMenu();
                    bugTrackerBar.parent().addClass('hidden');
                    expandContent();
                });
            }else{
                toggleMenu();
            }
        });

        $("#btn-show-bug-tracker").click(function(){
            reduceContent();
            bugTrackerBar.parent().removeClass('hidden');

            if (menu.hasClass('collapsed')){
                toggleMenu();
            }
            btnShowBugTracker.hide();
            bugTrackerBar.removeClass('hidden').animate({left:'0'});
        });

        $("#btn-hide-bug-tracker").click(function(){
            bugTrackerBar.animate({left:$('.bug-tracker-bar').width() * -1}, function(){
                bugTrackerBar.addClass("hidden");
                btnShowBugTracker.show();
                expandContent();
            });
        });
    })

})(jQuery);
