(function($) {

    "use strict";

    function dropdownShow(t) {
        var self, btEl, aEls;
        self = $(t.target);
        btEl = self.find(".btn");
        aEls = self.find("a");

        if (btEl.length === 0 || aEls.length === 0) { return; }

        btEl.data("toggleText", btEl.html());
        btEl.data("toggleClass", btEl.attr("class"));

        btEl.html("Select");
        btEl.attr("class", "btn no-select btn-fix-w-114 btn-primary-light_white dropdown-toggle");

        aEls.each(function() {
            var clsName;
            clsName = "";
            if ($(this).data("toggleText") === btEl.data("toggleText")) { clsName = "active"; }

            this.parentNode.className = clsName;
        });
    }
    function dropdownHide(t) {
        var self, btEl;
        self = $(t.target);
        btEl = self.find(".btn");

        if (btEl.length === 0) { return; }

        btEl.html(btEl.data("toggleText"));
        btEl.attr("class", btEl.data("toggleClass"));
    }
    function dropdownSelect(t) {
        var self, rootEl, btEl;
        self = $(t.target);
        rootEl = $(t.target.parentNode.parentNode.parentNode);

        btEl = rootEl.find(".btn");

        if (btEl.length === 0 ) { return; }
        if (self[0].tagName != "A") { self = self.find("a"); }

        btEl.data("toggleText", self.data("toggleText"));
        btEl.data("toggleClass", ("dropdown-toggle no-select btn btn-fix-w-114 btn-primary-"+self.data("toggleClass")));
    }


    $.fn.makeBezier = function() {
        var canvas = this.get(0);
        var context = canvas.getContext('2d');
        var data = $(canvas).data('points');

        var backgrounds = [
            '#e48275',//light red
            '#c2f5d6'//light green
        ];

        for(var i in data){
            var points = data[i];
            context.strokeStyle = backgrounds[i];
            context.beginPath();
            context.moveTo(points[0][0], points[0][1]);

            for (var i = 1; i < points.length - 2; i ++) {
                var xc = (points[i][0] + points[i + 1][0]) / 2;
                var yc = (points[i][1] + points[i + 1][1]) / 2;
                context.quadraticCurveTo(points[i][0], points[i][1], xc, yc);
            }
            context.quadraticCurveTo(points[i][0], points[i][1], points[i+1][0],points[i+1][1]);
            context.stroke();
            context.closePath();
        }
    };
    
    $(document).ready(function() {
        var dropdownToggle;
        dropdownToggle = $(".dropdown-toggle").parent();

        dropdownToggle.on("show.bs.dropdown", dropdownShow);
        dropdownToggle.on("hide.bs.dropdown", dropdownHide);
        dropdownToggle.find("li").on("click", dropdownSelect);


        $(".filter-item input").on("click",function(){
            $(this).parent().toggleClass("active");
        });
    

    });

})(jQuery);
