<?php

/* @var $this yii\web\View */

$this->title = 'Forgot password';
?>

<main class="box col-xs-12 forgot-password">
    <div class="box-header">
        <div class="text-bold text-center box-title">Reset password</div>
    </div>

        <input name="email" class="for-form center-placeholder text-center" placeholder="Recovery e-mail address"/>
        <div class="clearfix"></div>
    
    <div class="box-footer">
        <button type="button" class="btn btn-primary-blue btn-lg-w-100 btn-lg-h">Request a new password</button>
    </div>
</main>
<div class="clearfix"></div>
<p class="useful_links text-center">Oh, you remember now? <a href="#">Sign in then</a></p>