<?php

/* @var $this yii\web\View */

$this->title = 'Project 1';
?>

<div class="statistics">

    <div class="col-lg-4 statistics-box">
        <div class="row">
            <div class="box-legend-table wrapper-full">
            <h3 class="no-margin text-size-15">Number of bugs:</h3>
            <div class="table-responsive">
                <table class="table-legends table no-border text-left text-size-13">
                    <thead>
                    <tr>
                        <th colspan="2">Bug status:</th>
                        <th># of bugs:</th>
                        <th>% of project:</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td><span class="point-rect point-rect-light-red"></span></td>
                        <td>New</td>
                        <td>2</td>
                        <td>100%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-purple"></span></td>
                        <td>Confirmed</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-light-purple"></span></td>
                        <td>Acknowledged</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-pink"></span></td>
                        <td>In progress</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-lime"></span></td>
                        <td>Feedback</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-lime_2"></span></td>
                        <td>Resolved</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    <tr>
                        <td><span class="point-rect point-rect-light-green_3"></span></td>
                        <td>Closed</td>
                        <td>0</td>
                        <td>0%</td>
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
        </div>
    </div>
    <div class="col-lg-3 statistics-box">
        <div class="row">
            <div class="box-legend-table wrapper-full">
                <h3 class="no-margin text-size-15">Open bugs:</h3>
            </div>
            <div class="hr"></div>
            <div class="square text-center col-lg-6 col-xs-12 text-size-14">
                <div class="inner text-semibold">
                    <p>2</p>
                    <span>Critical</span>
                </div>
            </div>
            <div class="square no-border-right text-center col-lg-6 col-xs-12 text-size-14">
                <div class="inner text-semibold">
                    <p>2</p>
                    <span>Major</span>
                </div>
            </div>
            <div class="square text-center col-lg-6 col-xs-12 text-size-14">
                <div class="inner text-semibold">
                    <p>2</p>
                    <span>Minor</span>
                </div>
            </div>
            <div class="square no-border-right text-center col-lg-6 col-xs-12 text-size-14">
                <div class="inner text-semibold">
                    <p>2</p>
                    <span>Trivial</span>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-5 statistics-box">
        <div class="row">
        <div class="box-legend-table wrapper-full">
            <h3 class="no-margin text-size-15">Burndown chart:</h3>
        </div>
        <div class="text-center table-responsive">
            <canvas id="bezier-curve-1" width="580" height="227" data-points='[[[0,100],[115,50],[265,110],[580,75]],[[0,70],[115,90],[265,150],[580,75]]]'></canvas>
                <div class="legend">
                    <div class="toolbar-panel toolbar-panel-small mb-12">
                        <div class="toolbar-panel-cell">
                            <span class="point-rect point-rect-light-red"></span>
                        </div>
                        <div class="toolbar-panel-cell">
                            <span class="text-color-light-red_2">Bugs reported</span>
                        </div>
                    </div>
                    <div class="toolbar-panel toolbar-panel-small">
                        <div class="toolbar-panel-cell">
                            <span class="point-rect point-rect-light-green_2"></span>
                        </div>
                        <div class="toolbar-panel-cell">
                            <span class="text-color-light-green_2">Bugs addressed</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="hr"></div>
    <div class="col-lg-12">
        <div class="row">
            <div class="box-legend-table wrapper-full">
                <h3 class="no-margin text-size-15">Latest test runs:</h3>
            </div>
            <div class="table-responsive">
                    <table border="0" class="table text-size-13">
                        <thead>
                        <tr class="border-bottom border-top text-left">
                            <th>Version:</th>
                            <th class="text-left">Device:</th>
                            <th colspan="4" class="border-left">New bugs found:</th>
                            <th class="border-left" style="min-width: 140px">Date:</th>
                            <th>Passed:</th>
                            <th>Failed:</th>
                            <th>Could not test:</th>
                            <th>Remaining:</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr>
                            <td>1.0.2</td>
                            <td>
                                <a href="#" class="link link-violet text-border-bt">iPhone 5S (9.0.1)</a>
                            </td>
                            <td class="border-left text-center text-bold">1 critical</td>
                            <td class="text-center text-bold">0 major</td>
                            <td class="text-center text-bold">0 minor</td>
                            <td class="text-center text-bold">3 trivial</td>
                            <td class="border-left">18 Dec 15</td>
                            <td class="text-color-light-green_2">82%</td>
                            <td class="text-color-light-red_2">10%</td>
                            <td>7%</td>

                            <td>0.5%</td>
                        </tr>

                        <tr>
                            <td>1.0.2</td>
                            <td>
                                <a href="#" class="link link-violet text-border-bt">iPhone 5S (9.0.1)</a>
                            </td>
                            <td class="border-left text-center text-bold">1 critical</td>
                            <td class="text-center text-bold">0 major</td>
                            <td class="text-center text-bold">0 minor</td>
                            <td class="text-center text-bold">3 trivial</td>
                            <td class="border-left">18 Dec 15</td>
                            <td class="text-color-light-green_2">82%</td>
                            <td class="text-color-light-red_2">10%</td>
                            <td>7%</td>

                            <td>0.5%</td>
                        </tr>

                        <tr>
                            <td>1.0.2</td>
                            <td>
                                <a href="#" class="link link-violet text-border-bt">iPhone 5S (9.0.1)</a>
                            </td>
                            <td class="border-left text-center text-bold">1 critical</td>
                            <td class="text-center text-bold">0 major</td>
                            <td class="text-center text-bold">0 minor</td>
                            <td class="text-center text-bold">3 trivial</td>
                            <td class="border-left">18 Dec 15</td>
                            <td class="text-color-light-green_2">82%</td>
                            <td class="text-color-light-red_2">10%</td>
                            <td>7%</td>

                            <td>0.5%</td>
                        </tr>

                        <tr>
                            <td>1.0.2</td>
                            <td>
                                <a href="#" class="link link-violet text-border-bt">iPhone 5S (9.0.1)</a>
                            </td>
                            <td class="border-left text-center text-bold">1 critical</td>
                            <td class="text-center text-bold">0 major</td>
                            <td class="text-center text-bold">0 minor</td>
                            <td class="text-center text-bold">3 trivial</td>
                            <td class="border-left">18 Dec 15</td>
                            <td class="text-color-light-green_2">82%</td>
                            <td class="text-color-light-red_2">10%</td>
                            <td>7%</td>

                            <td>0.5%</td>
                        </tr>

                        <tr>
                            <td>1.0.2</td>
                            <td>
                                <a href="#" class="link link-violet text-border-bt">iPhone 5S (9.0.1)</a>
                            </td>
                            <td class="border-left text-center text-bold">1 critical</td>
                            <td class="text-center text-bold">0 major</td>
                            <td class="text-center text-bold">0 minor</td>
                            <td class="text-center text-bold">3 trivial</td>
                            <td class="border-left">18 Dec 15</td>
                            <td class="text-color-light-green_2">82%</td>
                            <td class="text-color-light-red_2">10%</td>
                            <td>7%</td>

                            <td>0.5%</td>
                        </tr>
                        </tbody>

                    </table>
            </div>
        </div>
    </div>
<script>
    $(document).ready(function() {
        $('#bezier-curve-1').makeBezier();
    })
</script>
<div class="clearfix"></div>


