<div class="wrapper-small">
    <h2>Pinned projects:</h2>
    <p class="text-color-light_brown_2 text-size-13">You haven’t pinned any projects to your dashboard. Use the pin icon
        <button class="sprite-button btn-primary-light-blue">
            <i class="sprite-dashboard sprite-dashboard-pin vertical-align-middle"></i>
        </button>
        to ensure your project is always shown here.</p>
</div>
<div class="wrapper">
    <h3>Most recently active projects: </h3>
    <div class="panel-darkly-white panel-project">
        <div class="project-toolbar">
            <div class="col-lg-4">
                    <div class="toolbar-panel toolbar-panel-height-25 no-margin-top block-center-xs">
                        <div class="toolbar-panel-cell">
                            <span class="text-size-13 text-bold">Website Redesign Project</span>
                        </div>
                        <div class="toolbar-panel-cell button-group-services block-center-xs block-static-xs">
                            <i class="sprite-dashboard sprite-dashboard-slack"></i>
                            <i class="sprite-dashboard sprite-dashboard-bb"></i>
                            <i class="sprite-dashboard sprite-dashboard-github"></i>
                        </div>
                </div>
            </div>
            <div class="col-lg-8 text-right">
                <div class="toolbar-panel toolbar-panel-height-25 no-margin-top toolbar-panel-small block-center-xs text-size-13">
                    <div class="toolbar-panel-cell">
                        <span>Your role:</span>
                        <a href="#" class="link link-blue-1">Project manager</a>
                    </div>

                    <div class="toolbar-panel-cell">
                        <span>Organization:</span>
                        <a href="#" class="link link-blue-1">QAOR</a>
                    </div>
                    <div class="clearfix visible-xs"></div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-chat vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-pin-gray vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-mobile vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-user vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-settings vertical-align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="wrapper">
            <div class="col-lg-6">
                <h3 class="text-center">Burndown chart:</h3>
                <div class="block-diagram text-center">
                    <canvas id="bezier-curve-1" width="396" height="198" data-points='[[[0,100],[115,50],[265,110],[398,75]],[[0,70],[115,90],[265,150],[398,75]]]'></canvas>
                    <div class="legend text-bold">
                        <div class="info">
                            <label>Open bugs</label>
                        </div>
                        <div class="info">
                            <label>Closed bugs</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                    <h3 class="text-center">Open critical bugs:</h3>
                    <div class="table-responsive">
                        <table class="table inverse-stripes first-row-background-odd large-titles text-size-11 text-left table-bordered stack-table">
                            <thead>
                                <tr>
                                    <th>Description:</th>
                                    <th>Component:</th>
                                    <th>Status:</th>
                                    <th>Found in:</th>
                                    <th>Assigned to:</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    </div>
                    <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Report a bug</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Go to the bug tracker</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="text-center">Last 2 completed test runs:</h3>
                <div class="block-diagram">
                    <div class="col-lg-6">
                        <div class="graph" style="height: 152px">
                            <div class="graph-table">
                                <div>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                </div>

                                <div>
                                    <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                    <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-12"></div>
                        <div>
                            <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consecteur adi...</a>
                            <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                        </div>
                        <div class="mb-12"></div>

                    </div>
                    <div class="col-lg-6">
                        <div class="graph" style="height: 152px">
                            <div class="graph-table">
                                <div>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                </div>

                                <div>
                                    <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                    <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-12"></div>

                        <div>
                            <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consecteur adi...</a>
                            <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                        </div>
                        <div class="mb-12"></div>

                    </div>
                    <div class="col-lg-12 text-center text-bold">
                        See more
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <h3 class="text-center">Last 3 started test runs:</h3>
                        <div class="table-responsive">
                            <table class="table inverse-stripes table-bordered first-row-background-odd large-titles text-size-11 text-left">
                                <thead>
                                <tr>
                                    <th style="width: 55%;">Test run name:</th>
                                    <th>Date:</th>
                                    <th>Version:</th>
                                </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>iPhone 6 (9.0.1)</td>
                                        <td>01-Oct-2015</td>
                                        <td>1.0.1</td>
                                    </tr>
                                    <tr>
                                        <td>iPhone 6 (9.0.1)</td>
                                        <td>01-Oct-2015</td>
                                        <td>1.0.1</td>
                                    </tr>
                                    <tr>
                                        <td>iPhone 6 (9.0.1)</td>
                                        <td>01-Oct-2015</td>
                                        <td>1.0.1</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Start a test run</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Go to the test plan</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="panel-darkly-white panel-project">
        <div class="project-toolbar">
            <div class="col-lg-4">
                <div class="toolbar-panel toolbar-panel-height-25 no-margin-top block-center-xs">
                    <div class="toolbar-panel-cell">
                        <span class="text-size-13 text-bold">Website Redesign Project</span>
                    </div>
                    <div class="toolbar-panel-cell button-group-services block-center-xs block-static-xs">
                        <i class="sprite-dashboard sprite-dashboard-slack"></i>
                        <i class="sprite-dashboard sprite-dashboard-bb"></i>
                        <i class="sprite-dashboard sprite-dashboard-github"></i>
                    </div>
                </div>
            </div>
            <div class="col-lg-8 text-right">
                <div class="toolbar-panel toolbar-panel-height-25 no-margin-top toolbar-panel-small block-center-xs text-size-13">
                    <div class="toolbar-panel-cell">
                        <span>Your role:</span>
                        <a href="#" class="link link-blue-1">Project manager</a>
                    </div>

                    <div class="toolbar-panel-cell">
                        <span>Organization:</span>
                        <a href="#" class="link link-blue-1">QAOR</a>
                    </div>
                    <div class="clearfix visible-xs"></div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-chat vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-pin-gray vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-mobile vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-user vertical-align-middle"></i>
                        </button>
                    </div>
                    <div class="toolbar-panel-cell">
                        <button class="sprite-button btn-primary-white">
                            <i class="sprite-dashboard sprite-dashboard-settings vertical-align-middle"></i>
                        </button>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="wrapper">
            <div class="col-lg-6">
                <h3 class="text-center">Burndown chart:</h3>
                <div class="block-diagram text-center">
                    <canvas id="bezier-curve-2" width="396" height="198" data-points='[[[0,100],[115,50],[265,110],[398,75]],[[0,70],[115,90],[265,150],[398,75]]]'></canvas>
                    <div class="legend text-bold">
                        <div class="info">
                            <label>Open bugs</label>
                        </div>
                        <div class="info">
                            <label>Closed bugs</label>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <h3 class="text-center">Open critical bugs:</h3>
                        <div class="table-responsive">
                            <table class="table inverse-stripes first-row-background-odd large-titles text-size-11 text-left table-bordered stack-table">
                                <thead>
                                <tr>
                                    <th>Description:</th>
                                    <th>Component:</th>
                                    <th>Status:</th>
                                    <th>Found in:</th>
                                    <th>Assigned to:</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                                <tr>
                                    <td>Hello, where can find a t...</td>
                                    <td>Checkout</td>
                                    <td class="text-color-light-red">New</td>
                                    <td>1.0.1</td>
                                    <td><a href="#" class="link link-blue-1">Natalie</a></td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Report a bug</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Go to the bug tracker</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <h3 class="text-center">Last 2 completed test runs:</h3>
                <div class="block-diagram">
                    <div class="col-lg-6">
                        <div class="graph" style="height: 152px">
                            <div class="graph-table">
                                <div>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                </div>

                                <div>
                                    <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                    <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-12"></div>
                        <div>
                            <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consecteur adi...</a>
                            <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                        </div>
                        <div class="mb-12"></div>

                    </div>
                    <div class="col-lg-6">
                        <div class="graph" style="height: 152px">
                            <div class="graph-table">
                                <div>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                    <span class="graph-line"></span>
                                </div>

                                <div>
                                    <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                    <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                </div>
                            </div>
                        </div>
                        <div class="mb-12"></div>

                        <div>
                            <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consecteur adi...</a>
                            <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                        </div>
                        <div class="mb-12"></div>

                    </div>
                    <div class="col-lg-12 text-center text-bold">
                        See more
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-lg-12">
                    <div class="row">
                        <h3 class="text-center">Last 3 started test runs:</h3>
                        <div class="table-responsive">
                            <table class="table inverse-stripes table-bordered first-row-background-odd large-titles text-size-11 text-left">
                                <thead>
                                <tr>
                                    <th style="width: 55%;">Test run name:</th>
                                    <th>Date:</th>
                                    <th>Version:</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>iPhone 6 (9.0.1)</td>
                                    <td>01-Oct-2015</td>
                                    <td>1.0.1</td>
                                </tr>
                                <tr>
                                    <td>iPhone 6 (9.0.1)</td>
                                    <td>01-Oct-2015</td>
                                    <td>1.0.1</td>
                                </tr>
                                <tr>
                                    <td>iPhone 6 (9.0.1)</td>
                                    <td>01-Oct-2015</td>
                                    <td>1.0.1</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Start a test run</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button type="button" class="btn btn-primary-blue_2">Go to the test plan</button>
                        </div>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="text-center">
        <button type="button" class="btn btn-primary-bordered-blue btn-lg-w">Load more projects</button>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#bezier-curve-1').makeBezier();
        $('#bezier-curve-2').makeBezier();
        $('.stack-table').stacktable();
    })
</script>