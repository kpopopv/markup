<?php

/* @var $this yii\web\View */

$this->title = 'Account settings 2';
?>

<div class="wrapper text-size-13">
   <div class="toolbar-panel mb-28">
       <div class="toolbar-panel-cell block-center-sm margin-vertical-5-sm">
           <div class="checkbox-move-wr">
               <input type="checkbox" class="move" id="checkboxAutosave" checked="">
               <label for="checkboxAutosave"></label>
           </div>
       </div>
       <div class="toolbar-panel-cell pg-16-left text-color-light_brown_2 block-center-sm">
           <label for="checkboxAutosave" class="font-weight-normal">Send me an e-mail when the status of a bug I’m involved in changes to Feedback.</label>
       </div>
   </div>
    <div class="toolbar-panel mb-28">
        <div class="toolbar-panel-cell block-center-sm margin-vertical-5-sm">
            <div class="checkbox-move-wr">
                <input type="checkbox" class="move" id="checkboxAutosave1" checked="">
                <label for="checkboxAutosave1"></label>
            </div>
        </div>
        <div class="toolbar-panel-cell pg-16-left text-color-light_brown_2 block-center-sm">
            <label for="checkboxAutosave1" class="font-weight-normal">Send me an e-mail when someone adds a comment to a bug I’m involved in.</label>
        </div>
    </div>
    <div class="toolbar-panel mb-28">
        <div class="toolbar-panel-cell block-center-sm margin-vertical-5-sm">
            <div class="checkbox-move-wr">
                <input type="checkbox" class="move" id="checkboxAutosave2" checked="">
                <label for="checkboxAutosave2"></label>
            </div>
        </div>
        <div class="toolbar-panel-cell pg-16-left text-color-light_brown_2 block-center-sm">
            <label for="checkboxAutosave2" class="font-weight-normal">Send me an e-mail when someone mentions me in a comment.</label>
        </div>
    </div>
    <div class="toolbar-panel mb-28">
        <div class="toolbar-panel-cell block-center-sm margin-vertical-5-sm">
            <div class="checkbox-move-wr">
                <input type="checkbox" class="move" id="checkboxAutosave3" checked="">
                <label for="checkboxAutosave3"></label>
            </div>
        </div>
        <div class="toolbar-panel-cell pg-16-left text-color-light_brown_2 block-center-sm">
            <label for="checkboxAutosave3" class="font-weight-normal">Send me an e-mail when someone assigns me a bug.</label>
        </div>
    </div>
</div>
