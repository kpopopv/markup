<?php

/* @var $this yii\web\View */

$this->title = 'Account settings 1';
?>

<div class="wrapper">
    <div class="text-center mb-80">
        <div class="text-icon mb-80 account-profile-user">
            <div class="account-profile-user-container">
                <img src="/themes/leantesting/images/profile-image.png"/>
                <i class="sprite-project sprite-project-edit-gray"></i>
            </div>
        </div>
        <h3 class="text-center pg-top-6 mb-22 text-title-2">Natalie Portman</h3>
        <p class="no-margin text-color-light_brown_2 text-medium text-size-13">(natalie)</p>
    </div>
    <div class="account-profile-fields mb-80">
        <div class="fields mb-36">
            <div class="row">
                <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>First name:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Natalie">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>Last name:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Portman">
                </div>
            </div>

            <div class="row">
                <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>E-mail:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="nat.portman@gmail.com">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>City:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Paris">
                </div>
            </div>
            <div class="row">
                <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>Country:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="France">
                </div>
            </div>
        </div>

        <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
            <div class="toolbar-panel-cell">
                <button type="button" class="btn btn-primary-darkly_white text-size-13">Cancel</button>
            </div>
            <div class="toolbar-panel-cell">
                <button type="button" class="btn btn-primary-blue_2 text-size-13">Save changes</button>
            </div>
        </div>

    </div>
    <h3 class="text-center pg-top-6 mb-22 text-title-2 mb-80">Connect with other services:</h3>

    <div class="account-servicies text-center clearfix">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="mb-28">
                <div class="account-service-logo mb-40">
                    <i class="sprite-dialogs sprite-dialogs-git"></i>
                </div>
                <p class="text-size-13 text-bold text-color-silver-blue no-margin mb-40">GitHub</p>
                <button type="button" class="btn btn-primary-blue_2 text-size-13">Connect</button>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="mb-28">
                <div class="account-service-logo mb-40">
                    <i class="sprite-dialogs sprite-dialogs-bit"></i>
                </div>
                <p class="text-size-13 text-bold text-color-silver-blue no-margin mb-40">Bitbucket</p>
                <button type="button" class="btn btn-primary-blue_2 text-size-13">Connect</button>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="mb-28">
                <div class="account-service-logo mb-40">
                    <i class="sprite-account sprite-account-google"></i>
                </div>
                <p class="text-size-13 text-bold text-color-silver-blue no-margin mb-40">Google</p>
                <button type="button" class="btn btn-primary-blue_2 text-size-13">Connect</button>
            </div>
        </div>
    </div>
</div>
