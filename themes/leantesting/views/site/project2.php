<?php

/* @var $this yii\web\View */

$this->title = 'Mobile SDK';
?>

<div class="text-center">
    <section class="wrapper">
        <h2>Lean Testing SDK</h2>
    </section>
    <p class="text-center text-size-14 no-margin mb-90 wrapper-horizontal">
        Allow your users and testers to <a href="#" class="link link-blue">report bugs directly from your app.</a>
    </p>
    <img src="/themes/leantesting/images/mobile-sdk.png" class="mobile-sdk-img"/>
    <section class="panel-light-blue_2 wrapper">
        <button type="button" class="btn btn-primary-blue text-bold">Download SDK</button>
    </section>
    <section class="wrapper">
        <div class="toolbar-panel">
            <div class="toolbar-panel-cell">
                <label>Language:</label>
            </div>
            <div class="toolbar-panel-cell">
                <div class="select select-primary text-italic-natural">
                    <select>
                        <option value="1">Swift</option>
                        <option value="2">Swift 1.0</option>
                    </select>
                </div>
            </div>
        </div>
    </section>
    <ol class="text-left col-lg-12 text-size-14">
        <li>
            <span>
                Open the <b href="#" class="link link-blue">LeanTesting-iOS.zip</b> file and extract the containing files (<b href="#" class="link link-blue">LeanTesting.bundle</b> and <b href="#" class="link link-blue">LeanTesting.framework</b>)
            </span>
        </li>
        <li>
            <span>
                In Xcode, open the folder containing the project onto which the SDK will be installed
            </span>
        </li>
        <li>
            <span>
                Drag and drop the Bundle and Framework files on the project folder
            </span>
        </li>
        <li>
            <span class="text-icon">
                Check the box with <b href="#" class="link link-blue">"Copy items if needed"</b><i class="sprite-project sprite-project-help"></i>
            </span>
        </li>
        <li>
            <span>
                Click Finish
            </span>
        </li>
        <li>
            <span>
                In your Objective-C bridging header file, import the framework header:
            </span>
            <code class="panel-darkly-white">
                #import <b class="text-color-light-red">&lt;LeanTesting/LeanTesting.h&gt;</b>
            </code>
        </li>
        <li>
            <span>
                Under <b href="#" class="link link-blue">Build Settings</b>, make sure the  <b href="#" class="link link-blue">Objective-C Bridging Header</b> build setting under  <b href="#" class="link link-blue">Swift Compiler - Code Generation</b> has a path to the
		        header.<br/> The path must be directed to the file itself, not the directory the file is in. The path should be relative to your project, similar to the
		        way your Info.plist path is specified in  <b href="#" class="link link-blue">Build Settings</b>.<br/> In most cases, you should not need to modify this setting
            </span>
        </li>
        <li><span>
                Make sure the "<b href="#" class="link link-blue">Defines Module</b>" build setting under " <b href="#" class="link link-blue">Packaging</b>" is set to  <b href="#" class="link link-blue">Yes</b>
            </span>
        </li>
        <li>
            <span>
                Open the file  <b href="#" class="link link-blue">AppDelegate.swift</b> and add the following line to the bool function  <b href="#" class="link link-blue">func application(...) -> Bool</b>:
            </span>
            <code class="panel-darkly-white">
                [LeanTesting activate SDKWithKey:<b class="text-color-light-red">@"0YwmS9dYqUCo7bhhGDKds8JD9tbVex7VzqKau1rM"</b> projectID:<b class="text-color-light-red">@"9547"</b>];
            </code>
        </li>
        <li>
            <span>Now you need to import the following frameworks:</span>
            <ul>
                <li>AssetsLibrary.framework</li>
                <li>CoreData.frameworks</li>
                <li>MobileCoreServices.framework</li>
                <li>SystemConfiguration.framework</li>
            </ul>
        </li>
        <li>
            <span>Create a new build with the SDK</span>
        </li>
        <li>
            <span>Enjoy!</span>
        </li>
    </ol>
    <div class="clearfix"></div>
</div>

