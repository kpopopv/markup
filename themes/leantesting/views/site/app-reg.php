<?php

/* @var $this yii\web\View */

$this->title = 'App registration';
?>

<section class="text-center">
    <h1 class="text-bold text-title-1">Register a new OAuth application:</h1>
</section>
<div class="fields">
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Application name<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Something users will recognize and trust">
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Application name<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural validation-success" placeholder="Something users will recognize and trust" value="Loremispum">
            <i class="sprite-app sprite-app-success"></i>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Homepage URL<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="The full URL to your application homepage">
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Homepage URL<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural validation-error" placeholder="The full URL to your application homepage" data-error-msg='"Homepage URL" cannot remain blank'>
            <i class="sprite-app sprite-app-error"></i>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Application&nbsp;description<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Displayed to all potential users of your application">
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label class="required-label">Authorization&nbsp;callback&nbsp;URL<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Your application's callback URL">
        </div>
    </div>
    <div class="clearfix uploader">
        <div class="col-lg-offset-3 col-lg-2 col-md-5 col-sm-12 text-right block-center-sm">
            <label class="required-label">Logo<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <button type="button" class="btn btn-primary-blue_2 text-size-11">Upload</button>
            <span class="text-bold text-color-light_brown_2 text-size-11">Recommended&nbsp;size&nbsp;:&nbsp;144x144&nbsp;px</span>
        </div>
    </div>
    <div class="clearfix uploader">
        <div class="col-lg-offset-3 col-lg-2 col-md-5 col-sm-12 text-right block-center-sm">
            <label class="required-label">Logo<sup>*</sup>:</label>
        </div>
        <div class="col-lg-4 col-md-6 clearfix table-panel">
                <div class="uploader-image-panel">
                    <img src="/themes/leantesting/images/upload-image-cap.png"/>
                    <i class="sprite-app sprite-app-uploader-cancel cancel-button"></i>
                </div>
                <div class="uploader-action-panel">
                    <button type="button" class="btn btn-primary-blue_2 text-size-11">Change</button>
                    <span class="text-bold text-color-light_brown_2 text-size-11">Recommended&nbsp;size&nbsp;:&nbsp;144x144&nbsp;px</span>
                </div>
        </div>
    </div>
    <div class="clearfix text-center">
        <div class="col-lg-12 col-md-12">
            <button type="button" class="btn btn-primary-blue_2 text-bold text-size-14 registration">Register application</button>
        </div>
    </div>
</div>

