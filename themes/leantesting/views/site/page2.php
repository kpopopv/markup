<?php

/* @var $this yii\web\View */

$this->title = 'Test plan';
?>

<div class="block block-top row">
    <div class="col-md-6">
        <div class="wrapper">
            <header class="text-title-1 text-bold">
                <span class="text-color-light_brown_2">“</span>QAOR Redesign<span class="text-color-light_brown_2">”</span> test plan
            </header>

            <div>
                <div class="text-color-light_brown_2 text-title-3 mb-36">
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit...
                </div>

                <ul class="panel-info text-color-light_brown_2">
                    <li>
                        <div class="panel-info_name text-bold">Version number:</div>
                        <div class="panel-info_value">1.0.2</div>
                    </li>
                    <li>
                        <div class="panel-info_name text-bold">Completed by:</div>
                        <div class="panel-info_value">
                            <a href="" class="link link-blue" href="#">Simon P</a>
                        </div>
                    </li>
                    <li class="panel-info_separator"></li>
                    <li>
                        <div class="panel-info_name text-bold">Start date/time:</div>
                        <div class="panel-info_value">18-Dec-2015 21:10:00 UTC</div>
                    </li>
                    <li>
                        <div class="panel-info_name text-bold">End date/time:</div>
                        <div class="panel-info_value">19-Dec-2015 21:10:00 UTC</div>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <div class="col-md-6">
        <div class="wrapper">
            <header class="text-title-2 text-bold">
                Detailed test run results:
            </header>

            <div class="graph">
                <div class="col-lg-6 col-md-12">
                    <div class="graph-table">
                        <div>
                            <span class="graph-line"></span>
                            <span class="graph-line"></span>
                            <span class="graph-line"></span>
                            <span class="graph-line"></span>
                            <span class="graph-line"></span>
                        </div>

                        <div>
                            <span class="graph-coll" data-n="60" style="height: 60%"></span>
                            <span class="graph-coll" data-n="20" style="height: 20%"></span>
                            <span class="graph-coll" data-n="10" style="height: 10%"></span>
                            <span class="graph-coll" data-n="10" style="height: 10%"></span>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 hidden-md hidden-sm hidden-xs">
                    <ul class="graph-info">
                        <li>
                            <span class="point-rect point-rect-light-green pull-left"></span>
                            <span class="text-color-light-green text-bold pg-16-left">PASS (60%)</span>
                        </li>
                        <li>
                            <span class="point-rect point-rect-light-red pull-left"></span>
                            <span class="text-color-light-red text-bold pg-16-left">FAIL (20%)</span>
                        </li>

                        <li>
                            <span class="point-rect point-rect-light-brown pull-left"></span>
                            <span class="text-color-light-brown text-bold pg-16-left">COULD NOT TEST (10%)</span>
                        </li>

                        <li>
                            <span class="point-rect point-rect-grey pull-left"></span>
                            <span class="text-color-grey text-bold pg-16-left">N/A (10%)</span>
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </div>
</div>

<div class="wrapper">
    <div class="row">
        <div class="col-md-4">
            <div class="text-title-2 text-bold">
                Test cases:
            </div>
        </div>

        <div class="col-md-8 col-sm-12">
            <nav class="navbar-page-nav">
                <ul class="navbar-page">
                    <li class="folding">
                        <a class="link link-blue-1" href="#">Expand / collapse all</a>
                    </li>
                    <li>
                        Show all:
                    </li>
                    <li>
                        <a class="link link-blue-1" href="#">Pass</a>
                    </li>
                    <li class="separator">
                        l
                    </li>
                    <li>
                        <a class="link link-blue-1" href="#">Fail</a>
                    </li>
                    <li class="separator">
                        l
                    </li>
                    <li>
                        <a class="link link-blue-1" href="#">Could not test</a>
                    </li>
                    <li class="separator">
                        l
                    </li>
                    <li>
                        <a class="link link-blue-1" href="#">N/A</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="wrapper">
    <div class="row">
        <div class=col-md-12>

            <!-- Component #1 -->
            <div class="panel-group" role="tablist" aria-multiselectable="true">
                <div class="panel-heading" role="tab"">
                    <div class="panel-title">
                        <div class="panel-darkly-brown">
                            <input type="checkbox" id="test2"><label for="test2"></label>
                            <a role="button"
                               data-toggle="collapse"
                               data-parent="#accordion"
                               href="#collapseRootOne"
                               aria-expanded="false"
                               class="text-bold pg-16 link link-white">Component #1:</a>
                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
                        </div>
                    </div>
                </div>

                <div id="collapseRootOne" class="panel-collapse collapse" role="tabpanel">
                    <div class="panel-body">

                        <!-- Case #1 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-child">
                                        <input type="checkbox" id="test4"><label for="test4"></label>
                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildOne"
                                           aria-expanded="false"
                                           class="text-bold text-color-violet pg-16 link">Case #1:</a>
                                        <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                        <ul class="panel-menu">
                                            <li>
                                                <div class="sprite sprite-comment"></div>
                                            </li>
                                            <li>
                                                <div class="sprite sprite-image-grey"></div>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-fix-w-114 btn-primary-light-green no-select">Pass</button>
                                            </li>

                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildOne"
                                                   class="sprite sprite-collapse-grey"
                                                   aria-expanded="false">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseChildOne" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <section>
                                                <header class="text-bold">Comment:</header>
                                                <div class="text-description text-size-13">
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae neque nec elit bibendum tristique. Sed iaculis scelerisque
                                                    eros, eget venenatis orci mollis quis. Cras vestibulum augue.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae
                                                    neque nec elit bibendum tristique. Sed iaculis scelerisque eros, eget venenatis orci mollis quis. Cras vestibulum augue.
                                                </div>
                                            </section>
                                        </div>

                                        <div class="col-md-2">
                                            <section>
                                                <header class="text-bold">Attachment:</header>
                                                <div class="attachment"></div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Case #1 -->

                        <!-- Case #2 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-child">
                                        <input type="checkbox" id="test141"><label for="test141"></label>
                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildTwo"
                                           aria-expanded="false"
                                           class="text-bold text-color-violet pg-16 link">Case #2:</a>
                                        <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                        <ul class="panel-menu">
                                            <li>
                                                <div class="sprite sprite-comment"></div>
                                            </li>
                                            <li>
                                                <button type="button" class="btn btn-fix-w-114 btn-primary-light-green no-select">Pass</button>
                                            </li>

                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildTwo"
                                                   class="sprite sprite-collapse-grey"
                                                   aria-expanded="false">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseChildTwo" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <section>
                                                <header class="text-bold">Comment:</header>
                                                <div class="text-description text-size-13">
                                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae neque nec elit bibendum tristique. Sed iaculis scelerisque
                                                    eros, eget venenatis orci mollis quis. Cras vestibulum augue.Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent vitae
                                                    neque nec elit bibendum tristique. Sed iaculis scelerisque eros, eget venenatis orci mollis quis. Cras vestibulum augue.
                                                </div>
                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Case #1 -->

                    </div>
                </div>
            </div>
            <!-- End component #1 -->

        </div>
    </div>
</div>