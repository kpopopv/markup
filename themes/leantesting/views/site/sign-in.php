<?php

/* @var $this yii\web\View */

$this->title = 'Sign in';
?>

<main class="box col-xs-12">
    <div class="box-header">
        <div class="text-bold text-center box-title">Sign in</div>
    </div>
    <div class="box-content">
        <input name="username" class="for-form center-placeholder text-center" placeholder="Username"/>
        <input name="password" class="for-form center-placeholder text-center" placeholder="Password"/>
        <ul class="sign-in-useful-links checkbox-custom-2 block-center-xs">
            <li class="block-center-xs-small no-margin-xs-small mb-12-xs-small"><input type="checkbox" id="remember_me" checked/><label for="remember_me">Remember me</label></li>
            <li class="block-center-xs-small no-margin-xs-small mb-12-xs-small"><a href="#">Forgot password?</a></li>
        </ul>
    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-primary-blue btn-lg-w-100 btn-lg-h">Sign in</button>
        <button type="button" class="btn btn-primary-darkly_white btn-lg-w-100 btn-lg-h text-color-light_brown_2">
            <div class="sprite-dialogs sprite-dialogs-git"></div>
               sign in with GitHub
        </button>
        <button type="button" class="btn btn-primary-darkly_white btn-lg-w-100 btn-lg-h text-color-light_brown_2">
            <div class="sprite-dialogs sprite-dialogs-bit"></div>
              sign in with Bitbucket
        </button>
    </div>
</main>
<div class="clearfix"></div>
<p class="useful_links text-center">Don’t have an account yet? <a href="#">Sign up now</a></p>