<?php

/* @var $this yii\web\View */

$this->title = 'Home';
?>

<div class="block row">
    <div class="col-md-10 col-md-offset-1">

        <div class="wrapper text-center mb-90" style="margin-top: 48px;padding-bottom: 27px;">
            <header class="text-title-1 text-bold" style="margin-bottom: 67px;">
                <span>You don’t have a test plan for this project yet.</span>
            </header>

            <div>
                <div class="sprite sprite-computer" style="margin-bottom: 39px"></div>
            </div>

            <div>
                <button type="button" class="btn btn-primary-green btn-bg" data-toggle="modal" data-target="#testModal">Start by creating one</button>
            </div>
        </div>
    </div>
</div>

<div class="hr"></div>

<div class="block row" style="margin-top: 44px">
    <div class="col-md-12">
        <section class="wrapper text-center" style="padding-bottom: 11px">
            <header class="text-title-1 text-bold">
                Here’s how test plans work:
            </header>
        </section>
    </div>

    <div class="col-lg-4 text-center mb-36">
        <div class="sprite sprite-illu1 mb-80" style="margin-top: 13px;"></div>

        <section>
            <header class="text-title-1 text-bold mb-80">
                Create a test plan for your project
            </header>

            <div class="text-title-4 text-color-darkly-brown">
                <div class="mb-36">
                    A test plan is an ensemble of test cases
                    organized around the various components
                    of your project.
                </div>

                <div>
                    You can add as much or as little
                    content as you want for each test case,
                    including preconditions, expected results
                    and steps to execute.
                </div>
            </div>
        </section>

    </div>
    <div class="col-lg-4 text-center mb-36">
        <div class="sprite sprite-illu2 mb-45"></div>

        <section>
            <header class="text-title-1 text-bold mb-40">
                Create a test run
            </header>

            <div class="text-title-4 text-color-darkly-brown">
                <div class="mb-36">
                    Select the test cases you’ll be testing
                    from the test plan.
                </div>

                <div>
                    You can view test cases in compact view or
                    long form, update several test cases at once
                    and report bugs directly from the interface.
                </div>
            </div>
        </section>

    </div>
    <div class="col-lg-4 text-center mb-36">
        <div class="sprite sprite-illu3 mb-80" style="margin-top: 30px;"></div>

        <section>
            <header class="text-title-1 text-bold mb-80">
                Consult your result in a glance
            </header>

            <div class="text-title-4 text-color-darkly-brown">
                <div class="mb-36">
                    Visual charts convey how close
                    your project is to completion.
                </div>

                <div>
                    Get project activity, pass rates and
                    number of new bugs reported all on
                    the same dashboard.
                </div>
            </div>
        </section>

    </div>

</div>


<div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="testModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close pull-right sprite sprite-close" data-dismiss="modal" aria-label="Close"></button>
                <div class="text-bold modal-title">
                    Start a new test run: <span class="text-col text-color-light_brown_2">“</span>QAOR Redesign<span class="text-col text-color-light_brown_2">”</span>
                </div>
            </div>
            <div class="hr"></div>
            <div class="modal-body">

                <div class="modal-panel-nav">
                    <div class="row mb-45">
                        <div class="col-md-offset-1 col-md-10">
                            <div class="col-lg-5 mb-12">
                                <div class="row text-center_v"">
                                    <div class="col-lg-6">
                                        <span class="text-title-4 text-bold">Name your test run:</span>
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="text" class="for-form" placeholder="Enter name here...">
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-7">
                                <div class="row text-center_v">
                                    <div class="col-lg-4">
                                        <span class="text-title-4 text-bold">Version number:</span>
                                    </div>
                                    <div class="col-lg-3 mb-12">
                                        <div class="select select-primary">
                                            <select>
                                                <option value="1">1.0.1</option>
                                                <option value="2">1.0.2</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-lg-5">
                                        <a class="text-bold link link-violet">+ Add a new version number</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="mb-45">
                        <div class="text-center">
                            <span class="text-title-4 text-bold">Select the test cases you’d like to include in this test run:</span>

                            <ul class="menu-select">
                                <li class="select"><a href="#" class="link link-light_brown_2">Select all</a></li>
                                <li><a href="#" class="link link-light_brown_2">Select all critical test cases</a></li>
                                <li><a href="#" class="link link-light_brown_2">Select all critical & high priority test cases</a></li>
                                <li><a href="#" class="link link-light_brown_2">Deselect all</a></li>
                            </ul>

                        </div>
                    </div>
                </div>

                <!-- Component #1 -->
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel-heading" role="tab"">
                        <div class="panel-title">
                            <div class="panel-darkly-brown">
                                <input type="checkbox" id="test2"><label for="test2"></label>
                                <a role="button"
                                   data-toggle="collapse"
                                   data-parent="#accordion"
                                   href="#collapseRootOne"
                                   aria-expanded="true"
                                   class="text-bold pg-16 link link-white">Component #1:</a>
                                <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
                            </div>
                        </div>
                    </div>

                    <div id="collapseRootOne" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">

                            <!-- Case #1 -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-darkly-white panel-child">
                                            <input type="checkbox" id="test5"><label for="test5"></label>
                                            <span class="text-bold pg-16">Case #1:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                            <div class="indicator indicator-hor pull-right" data-n="2">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #1 -->

                            <!-- Case #2 -->
                            <div class="panel-group" role="tablist">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-darkly-white panel-child">
                                            <input type="checkbox" id="test4"><label for="test4"></label>
                                            <span class="text-bold pg-16">Case #2:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                            <div class="indicator indicator-hor pull-right" data-n="1">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #2 -->

                        </div>
                    </div>
                </div>
                <!-- End component #1 -->

                <!-- Component #2 -->
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel-heading" role="tab"">
                        <div class="panel-title">
                            <div class="panel-darkly-brown">
                                <input type="checkbox" id="test211" checked><label for="test211"></label>
                                <a role="button"
                                   data-toggle="collapse"
                                   data-parent="#accordion"
                                   href="#collapseRootTwo"
                                   aria-expanded="true"
                                   class="text-bold pg-16 link link-white">Component #2:</a>
                                <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
                            </div>
                        </div>
                    </div>

                    <div id="collapseRootTwo" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">

                            <!-- Case #1 -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-light-blue panel-child">
                                            <input type="checkbox" id="test53" checked><label for="test53"></label>
                                            <span class="text-bold pg-16">Case #2:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>


                                            <div class="indicator indicator-hor pull-right" data-n="4">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #1 -->

                            <!-- Case #2 -->
                            <div class="panel-group" role="tablist">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-light-blue panel-child">
                                            <input type="checkbox" id="test41" checked><label for="test41"></label>
                                            <span class="text-bold pg-16">Case #2:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                            <div class="indicator indicator-hor pull-right" data-n="3">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #2 -->
                        </div>
                    </div>
                </div>
                <!-- End component #2 -->

                <!-- Component #3 -->
                <div class="panel-group" role="tablist" aria-multiselectable="true">
                    <div class="panel-heading" role="tab"">
                        <div class="panel-title">
                            <div class="panel-darkly-brown">
                                <input type="checkbox" id="test2111"><label for="test2111"></label>
                                <a role="button"
                                   data-toggle="collapse"
                                   data-parent="#accordion"
                                   href="#collapseRootThee"
                                   aria-expanded="true"
                                   class="text-bold pg-16 link link-white">Component #3:</a>
                                <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
                            </div>
                        </div>
                    </div>

                    <div id="collapseRootThee" class="panel-collapse collapse in" role="tabpanel">
                        <div class="panel-body">

                            <!-- Case #1 -->
                            <div class="panel-group" role="tablist" aria-multiselectable="true">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-darkly-white panel-child">
                                            <input type="checkbox" id="test513"><label for="test513"></label>
                                            <span class="text-bold pg-16">Case #1:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                            <div class="indicator indicator-hor pull-right" data-n="0">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #1 -->

                            <!-- Case #2 -->
                            <div class="panel-group" role="tablist">
                                <div class="panel-heading" role="tab"">
                                    <div class="panel-title">
                                        <div class="panel-darkly-white panel-child">
                                            <input type="checkbox" id="test411"><label for="test411"></label>
                                            <span class="text-bold pg-16">Case #2:</span>
                                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                            <div class="indicator indicator-hor pull-right" data-n="4">
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                                <div class="indicator_scale"></div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- End case #2 -->
                        </div>
                    </div>
                </div>
                <!-- End component #3 -->

            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-darkly_white" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue">Save & Start</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
