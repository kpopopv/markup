<?php

/* @var $this yii\web\View */

$this->title = 'Test plan edit';
?>

<div class="block block-top row">
    <div class="col-lg-5">
        <div class="wrapper">
            <header class="text-title-1 text-bold">
                <span class="text-color-light_brown_2">“</span>QAOR Redesign<span class="text-color-light_brown_2">”</span> test plan
            </header>

            <div>
                <div class="text-color-light_brown_2 text-title-3 mb-36">
                    <span class="text-color-blue text-bold">12</span> components&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;<span class="text-color-blue text-bold">144</span> test cases
                </div>

                <ul class="panel-info panel-info-sm text-color-light_brown_2">
                    <li>
                        <div class="panel-info_name text-bold">Created:</div>
                        <div class="panel-info_value">18-Dec-2015 21:10:00 UTC by <a href="#" class="link link-blue">Simon P</a></div>
                    </li>
                    <li>
                        <div class="panel-info_name text-bold">Last updated:</div>
                        <div class="panel-info_value">19-Dec-2015 21:10:00 UTC by <a href="#" class="link link-blue">Simon P</a></div>
                    </li>
                </ul>
                <div class="mb-36"></div>

                <div>
                    <button type="button" class="btn btn-primary-blue">Start a new test run</button>
                    &nbsp;&nbsp;
                    <button type="button" class="btn btn-primary-darkly_white">Edit test plan</button>
                </div>

            </div>
        </div>
    </div>
    <div class="col-lg-7">
        <div class="wrapper">
            <div class="mb-22 hidden visible-xs visible-sm"></div>
            <header class="text-title-1 text-bold">
                Last 3 test runs:
            </header>

            <div class="row">

                <div class="col-lg-3">
                    <div class="graph" style="height: 152px">
                        <div class="graph-table">
                            <div>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                            </div>

                            <div>
                                <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-12"></div>

                    <div>
                        <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consecteur adi...</a>
                        <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                    </div>
                    <div class="mb-12"></div>

                </div>

                <div class="col-lg-3">

                    <div class="graph" style="height: 152px">
                        <div class="graph-table">
                            <div>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                            </div>

                            <div>
                                <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                            </div>
                        </div>
                    </div>
                    <div class="mb-12"></div>

                    <div>
                        <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor...</a>
                        <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                    </div>
                    <div class="mb-12"></div>

                </div>

                <div class="col-lg-3">

                    <div class="graph" style="height: 152px">
                        <div class="graph-table">
                            <div>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                                <span class="graph-line"></span>
                            </div>

                            <div>
                                <span class="graph-coll" data-n="60" style="height: 60%"></span>
                                <span class="graph-coll" data-n="20" style="height: 20%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                                <span class="graph-coll" data-n="10" style="height: 10%"></span>
                            </div>
                        </div>
                    </div>

                    <div class="mb-12"></div>

                    <div>
                        <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor...</a>
                        <div class="text-color-light_brown_2 text-sx">Ver. #1.2.3&nbsp;&nbsp;·&nbsp;&nbsp;18-Dec-2015</div>
                    </div>
                    <div class="mb-12"></div>

                </div>

                <div class="col-lg-3 hidden-md hidden-sm hidden-xs">
                    <div>
                        <div class="point-rect point-rect-light-green"></div>
                        <div class="text-color-light-green text-bold">PASS</div>
                        <div class="mb-12"></div>
                    </div>
                    <div>
                        <div class="point-rect point-rect-light-red"></div>
                        <div class="text-color-light-red text-bold">FAIL</div>
                        <div class="mb-12"></div>
                    </div>
                    <div>
                        <div class="point-rect point-rect-light-brown"></div>
                        <div class="text-color-light-brown text-bold">COULD NOT TEST</div>
                        <div class="mb-12"></div>
                    </div>
                    <div>
                        <div class="point-rect point-rect-grey"></div>
                        <div class="text-color-grey text-bold">N/A</div>
                        <div class="mb-12"></div>
                    </div>

                </div>

            </div>

        </div>
    </div>
</div>

<div class="wrapper wrapper-border-bt">
    <span class="text-bold text-title-2">
        Latest test runs:
    </span>

    <div class="pull-right">
        <button class="btn btn-primary-blue">See more</button>
    </div>
</div>

<section class="table-responsive mb-45 overflow-x-hidden-1280">
    <table border="0" class="table text-size-13">
        <thead>
        <tr class="border-bottom">
            <th></th>
            <th></th>
            <th colspan="4" class="border-left">New bugs found:</th>
            <th class="border-left"></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <tr class="border-bottom">
            <th>V:</th>
            <th class="text-left">Test run name:</th>
            <th class="border-left">Critical:</th>
            <th>Major:</th>
            <th>Minor:</th>
            <th>Trivial:</th>
            <th class="border-left" style="min-width: 140px">Date:</th>
            <th>Passed:</th>
            <th>Failed:</th>
            <th>CNT:</th>
            <th>N/A:</th>
            <th>Remaining:</th>
        </tr>
        </thead>

        <tbody>
            <tr>
                <td>1.0.2</td>
                <td>
                    <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consectetur adipiscing...</a>
                </td>
                <td class="border-left text-center text-bold">1</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">3</td>
                <td class="border-left">18 Dec 15</td>
                <td class="text-color-light-green_2">82%</td>
                <td class="text-color-light-red_2">10%</td>
                <td>7%</td>
                <td>1%</td>
                <td>0.5%</td>
            </tr>

            <tr>
                <td>1.0.2</td>
                <td>
                    <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consectetur adipiscing...</a>
                </td>
                <td class="border-left text-center text-bold">1</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">3</td>
                <td class="border-left">18 Dec 15</td>
                <td class="text-color-light-green_2">82%</td>
                <td class="text-color-light-red_2">10%</td>
                <td>7%</td>
                <td>1%</td>
                <td>0.5%</td>
            </tr>

            <tr>
                <td>1.0.2</td>
                <td>
                    <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consectetur adipiscing...</a>
                </td>
                <td class="border-left text-center text-bold">1</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">3</td>
                <td class="border-left">18 Dec 15</td>
                <td class="text-color-light-green_2">82%</td>
                <td class="text-color-light-red_2">10%</td>
                <td>7%</td>
                <td>1%</td>
                <td>0.5%</td>
            </tr>

            <tr>
                <td>1.0.2</td>
                <td>
                    <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consectetur adipiscing...</a>
                </td>
                <td class="border-left text-center text-bold">1</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">3</td>
                <td class="border-left">18 Dec 15</td>
                <td class="text-color-light-green_2">82%</td>
                <td class="text-color-light-red_2">10%</td>
                <td>7%</td>
                <td>1%</td>
                <td>0.5%</td>
            </tr>

            <tr>
                <td>1.0.2</td>
                <td>
                    <a href="#" class="link link-violet text-border-bt">Lorem ipsum dolor sit amet, consectetur adipiscing...</a>
                </td>
                <td class="border-left text-center text-bold">1</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">0</td>
                <td class="text-center text-bold">3</td>
                <td class="border-left">18 Dec 15</td>
                <td class="text-color-light-green_2">82%</td>
                <td class="text-color-light-red_2">10%</td>
                <td>7%</td>
                <td>1%</td>
                <td>0.5%</td>
            </tr>
        </tbody>

    </table>
</section>

<div class="wrapper">
    <header class="text-title-1 text-bold">
        Latest modifications:
    </header>
</div>


<div class="wrapper">

    <div class="panel-white mb-22">
        <ul class="panel-menu panel-menu_left">
            <li>
                <div class="avatar-wrapper">
                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                </div>
            </li>
            <li>
                Friday, 18-Dec-2015 21:10:00 UTC&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;<a class="link link-violet" href="#">Simon P</a>&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;added&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;
                <a href="#" class="link link-blue">test case #149 {action: lorem ipsum dolor sit amet}</a>
            </li>
            <li></li>
        </ul>
    </div>

    <div class="panel-white mb-22">
        <ul class="panel-menu panel-menu_left">
            <li>
                <div class="avatar-wrapper">
                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                </div>
            </li>
            <li>
                Friday, 18-Dec-2015 21:10:00 UTC&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;<a class="link link-violet" href="#">Simon P</a>&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;modified&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;
                <a href="#" class="link link-blue">test case #121 {action: lorem ipsum dolor sit amet}</a>
            </li>
            <li></li>
        </ul>

        <div class="row">
            <div class="col-lg-6">
                <div class="col-lg-offset-2 col-lg-8">
                    <div class="mb-40"></div>
                    <section class="text-size-13">
                        <header class="text-bold mb-28">Original value:</header>
                        <div class="text-color-light_brown_2 mb-28">Preconditions</div>
                        <div class="text-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Praesent vitae neque nec elit bibendum
                            tristique. Sed iaculis scelerisque eros, eget
                            venenatis orci mollis quis. Cras vestibulum augue.
                        </div>
                    </section>
                </div>
            </div>


            <div class="col-lg-6">
                <div class="col-lg-offset-2 col-lg-8">
                    <div class="mb-40"></div>
                    <section class="text-size-13">
                        <header class="text-bold mb-28">New value:</header>
                        <div class="text-color-light_brown_2 mb-28">Preconditions</div>
                        <div class="text-description">
                            Lorem ipsum dolor sit amet, consectetur adipiscing
                            elit. Praesent vitae neque nec elit bibendum
                            tristique. Sed iaculis scelerisque eros, eget
                            venenatis orci mollis quis. Cras vestibulum augue.
                        </div>
                    </section>
                </div>
            </div>
        </div>
    </div>

    <div class="panel-white mb-22">
        <ul class="panel-menu panel-menu_left">
            <li>
                <div class="avatar-wrapper">
                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                </div>
            </li>
            <li>
                Friday, 18-Dec-2015 21:10:00 UTC&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;<a class="link link-violet" href="#">Simon P</a>&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;added&nbsp;&nbsp;&nbsp;·&nbsp;&nbsp;&nbsp;
                <a href="#" class="link link-blue">test case #149 {action: lorem ipsum dolor sit amet}</a>
            </li>
            <li></li>
        </ul>
    </div>

</div>