<?php

/* @var $this yii\web\View */

$this->title = 'Apps';
?>

<section class="text-center">
    <h1 class="text-bold text-title-1">Developer applications:</h1>
</section>
<section class="text-center panel-light-bg">
    <button type="button" class="btn btn-primary-blue text-bold text-size-15 text-title-1">Register an application</button>
    <p>to generate OAuth tokens.</p>
</section>
<section class="text-center">
    <h1 class="text-bold text-title-1">Authorized applications:</h1>
    <p class="wrapper-horizontal">You have no applications authorized to access your account.</p>
</section>
<section class="text-center">
    <h1 class="text-bold text-title-1">Trusted applications:</h1>
    <p class="wrapper-horizontal">These are applications developed and owned by Crowdsourced Testing.<br/>
        They have full access to your Leantesting account.</p>
    <ul>
        <li class="row">
            <div class="col-md-4 col-xs-12">
                <span>Leantesting iOS SDK</span>
            </div>
            <div class="col-md-8 col-xs-12">
                <button type="button" class="btn">Revoke</button>
            </div>
        </li>
        <li class="row">
                <div class="col-md-4 col-xs-12">
                    <span>Leantesting iOS SDK</span>
                </div>
                <div class="col-md-8 col-xs-12">
                    <button type="button" class="btn">Revoke</button>
                </div>
        </li>
        <li class="row">
            <div class="col-md-4 col-xs-12">
                <span>Leantesting iOS SDK</span>
            </div>
            <div class="col-md-8 col-xs-12">
                <button type="button" class="btn">Revoke</button>
            </div>
        </li>
    </ul>
</section>