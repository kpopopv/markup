<?php

/* @var $this yii\web\View */

$this->title = 'Ui kit';
?>

<div style="padding: 25px">
    <div>
        <button type="button" class="btn btn-fix-w-114 btn-primary-light-green">Pass</button>
        <button type="button" class="btn btn-fix-w-114 btn-primary-red">Fail</button>
        <button type="button" class="btn btn-fix-w-114 btn-primary-light-brown">CNT</button>
        <button type="button" class="btn btn-fix-w-114 btn-primary-grey">N/A</button>
        <button type="button" class="btn btn-fix-w-114 btn-primary-light_white">Select</button>
        <button type="button" class="btn btn-fix-w-114 btn-primary-blue">Report bug</button>
    </div>

    <br>
    <div>
        <button type="button" class="btn btn-primary-darkly_white">Edit selected</button>
        <button type="button" class="btn btn-lg btn-primary-blue">Save & start</button>
    </div>
    <br>
    <div>
        <div class="btn-group">
            <button type="button" class="dropdown-toggle btn btn-fix-w-114 btn-primary-light-green" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pass</button>
            <ul class="dropdown-menu">
                <li><a href="#" data-toggle-text="Pass" data-toggle-class="light-green">Pass</a></li>
                <li><a href="#" data-toggle-text="Fail" data-toggle-class="red">Fail</a></li>
                <li><a href="#" data-toggle-text="CNT" data-toggle-class="light-brown">Could not test</a></li>
                <li><a href="#" data-toggle-text="N/A" data-toggle-class="grey">Not applicable</a></li>
            </ul>
        </div>
    </div>
    <br>
    <button type="button" class="btn btn-primary btn-lg" data-toggle="modal" data-target="#testModal">Launch demo modal</button>
    <br>

    <br>
    <div class="indicator indicator-hor" data-n="3">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>

    <br>
    <div class="indicator" data-n="0">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>
    <br>
    <div class="indicator" data-n="1">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>
    <br>
    <div class="indicator" data-n="2">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>
    <br>
    <div class="indicator" data-n="3">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>
    <br>
    <div class="indicator" data-n="4">
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
        <div class="indicator_scale"></div>
    </div>
    <br>

    <div>
        <div class="select select-primary">
            <select>
                <option value="1">Ios</option>
                <option value="2">Android</option>
                <option value="3">Window</option>
            </select>
        </div>
    </div>
    <br>

    <div>
        <p>
            <input type="checkbox" id="test1" />
            <label for="test1" class="text-color-light_brown_2">Select all</label>
        </p>
    </div>
    <br>

    <div>
        <textarea class="for-form" placeholder="Lorem impsum dolor..."></textarea>
    </div>
    <br>

    <div class="row">
        <div class="col-md-3">
            <div class="panel-gradient-blue">
                3.	Lorem impsum dolor...
            </div>
        </div>
    </div>
    <br>

    <div>
        <div class="panel-darkly-brown">
            <input type="checkbox" id="test2"><label for="test2"></label>
            <span class="text-bold pg-16">Component #1:</span><span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
        </div>
    </div>
    <br>

    <div>
        <div class="panel-darkly-white panel-child">
            <input type="checkbox" id="test3"><label for="test3"></label>
            <span class="text-bold pg-16">Component #1:</span><span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

            <div class="indicator indicator-hor pull-right" data-n="1">
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
            </div>
        </div>
    </div>
    <br>

    <div>
        <div class="panel-light-blue panel-child">
            <input type="checkbox" id="test4" checked><label for="test4"></label>
            <span class="text-bold pg-16">Component #1:</span><span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

            <div class="indicator indicator-hor pull-right" data-n="3">
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
                <div class="indicator_scale"></div>
            </div>
        </div>
    </div>
    <br>

    <div class="graph">
        <div class="col-lg-6 col-md-12">
            <div class="graph-table">
                <div>
                    <span class="graph-line"></span>
                    <span class="graph-line"></span>
                    <span class="graph-line"></span>
                    <span class="graph-line"></span>
                    <span class="graph-line"></span>
                </div>

                <div>
                    <span class="graph-coll" data-n="60" style="height: 60%"></span>
                    <span class="graph-coll" data-n="20" style="height: 20%"></span>
                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                    <span class="graph-coll" data-n="10" style="height: 10%"></span>
                </div>
            </div>
        </div>

        <div class="col-md-6 hidden-md hidden-sm hidden-xs">
            <ul class="graph-info">
                <li>
                    <span class="point-rect point-rect-light-green pull-left"></span>
                    <span class="text-color-light-green text-bold pg-16-left">PASS (60%)</span>
                </li>
                <li>
                    <span class="point-rect point-rect-light-red pull-left"></span>
                    <span class="text-color-light-red text-bold pg-16-left">FAIL (20%)</span>
                </li>

                <li>
                    <span class="point-rect point-rect-light-brown pull-left"></span>
                    <span class="text-color-light-brown text-bold pg-16-left">COULD NOT TEST (10%)</span>
                </li>

                <li>
                    <span class="point-rect point-rect-grey pull-left"></span>
                    <span class="text-color-grey text-bold pg-16-left">N/A (10%)</span>
                </li>
            </ul>
        </div>
    </div>
</div>

<div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="testModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="sprite sprite-close close pull-right" data-dismiss="modal" aria-label="Close"></button>
                <div class="text-bold modal-title">
                    Start a new test run: <span class="text-col text-color-light_brown_2">“</span>QAOR Redesign<span class="text-col text-color-light_brown_2">”</span>
                </div>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-darkly_white" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue">Save changes</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>