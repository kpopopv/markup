<?php

/* @var $this yii\web\View */

$this->title = 'Account settings';
?>

<div class="wrapper">
    <h3 class="text-center pg-top-6 mb-40 text-title-2">Organization information:</h3>
    <div class="fields text-size-13">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>Name:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Crowdsourced Testing">
            </div>
        </div>
        <div class="row">
            <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>Alias:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="crowdsourced.teantesting.com">
            </div>
        </div>

        <div class="row">
            <div class="col-lg-offset-3 col-lg-1 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>URL:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="http://crowdtesting.com">
            </div>
        </div>
        <div class="row uploader">
            <div class="col-lg-offset-3 col-lg-1  col-md-5 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>Logo:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 action-container block-center-sm">
                <button type="button" class="btn btn-primary-blue_2 text-size-11">Upload</button>
                <span class="text-bold text-color-light_brown_2 text-size-11 pg-16-left">We recommend .gif or .png files (150x150px).</span>
            </div>
        </div>
        <div class="row uploader">
            <div class="col-lg-offset-3 col-lg-1  col-md-5 col-sm-12 text-right block-center-sm ">
                <div class="row">
                    <label>Current logo:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12 col-xs-12">
                <div class="uploader-image-panel text-center">
                    <img src="/themes/leantesting/images/upload-image-cap.png">
                    <i class="sprite-app sprite-app-uploader-cancel cancel-button pull-right"></i>
                </div>
            </div>
        </div>
        <h3 class="text-center pg-top-20 mb-80 text-title-2">Usage:</h3>
        <div class="mb-22">
            <div class="row">
                <div class="col-lg-offset-2 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                    <div class="row">
                        <label>Storage used:</label>
                    </div>
                </div>
                <div class="col-lg-4 col-md-6">
                    <div class="progress">
                        <div class="progress-bar panel-gradient-blue text-center" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%;">
                            <b>50 %</b> (501.8GB of 1TB)
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mb-28">
            <div class="col-lg-offset-2 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>Integrations&nbsp;used:</label>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-12">
                <div class="integrations-buttons text-center">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">
                            <div class="row">
                                <i class="sprite-account sprite-account-slack"></i>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-3 col-sm-4 col-xs-3">
                            <div class="row">
                                <i class="sprite-account sprite-account-github"></i>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-4 col-xs-5">
                            <div class="row">
                                <i class="sprite-account sprite-account-hipchat"></i>
                            </div>
                        </div>
                </div>
            </div>
        </div>
        <p class="text-center text-color-light_brown_2"><b>96 integrations used</b> (∞ more available)</p>
        <h3 class="text-center pg-top-20 mb-80 text-title-2">Subscriptions:</h3>
        <div class="panel-light-green_4 text-center subscription-panel text-size-13">
            Next billing date: 30th November 2016
        </div>
        <h4 class="text-center pg-top-20 mb-80 text-size-14">Billing information:</h4>
        <div class="row">
            <div class="col-lg-offset-2 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
                <div class="row">
                    <label>Credit card:</label>
                </div>
            </div>
            <div class="col-lg-5 col-md-6">
                <div class="row">
                    <div class="credit-card">
                        <div class="col-lg-10">
                            <div class="input-container">
                                <i class="sprite-account sprite-account-cc"></i>
                                <input type="text" class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="xxxx   xxxx   xxxx   9777">
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div class="block-center-lg margin-vertical-5-lg">
                                <div class="row">
                                    <button class="btn btn-primary-blue_2">Update</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


</div>
</div>

    <div class="table-responsive mb-80">
        <table border="0" class="table inverse-stripes active-icons subscriptions-list text-size-13">
            <tbody>
            <tr>
                <td>
                    <div class="text-size-13 mb-12 text-bold ">Custom bug types and statuses</div>
                    <span class="text-color-light_brown_2">
                        Create your own bug types and statuses to better suit your workflow.
                    </span>
                </td>
                <td class="hidden-sm hidden-xs"></td>
                <td class="text-center text-bold">
                    <a href="#" class="link link-blue">You already have this, yay.</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="text-size-13 mb-12 text-bold ">Unlimited integrations</div>
                    <span class="text-color-light_brown_2">
                        Unlimited integrations (Slack, Github) for all your projects.
                    </span>
                </td>
                <td class="hidden-sm hidden-xs"></td>
                <td class="text-center text-bold">
                    <a href="#" class="link link-blue">You already have this, yay.</a>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="text-size-13 mb-12 text-bold ">25GB storage</div>
                    <span class="text-color-light_brown_2">
                        Extra storage for your files.
                    </span>
                </td>
                <td class="hidden-sm hidden-xs"></td>
                <td class="text-right text-bold">
                    <div class="mb-22">
                        <button class="btn btn-primary-blue_2">Subscribe</button>
                    </div>
                    <span class="text-color-light_brown_2">for only 5$ / month</span>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="text-size-13 mb-12 text-bold ">Advanced e-mail notifications</div>
                    <span class="text-color-light_brown_2">
                        Enables you and your team to receive more e-mail notifications based on project activity.
                    </span>
                </td>
                <td class="hidden-sm hidden-xs"></td>
                <td class="text-right text-bold">
                    <div class="mb-22">
                        <button class="btn btn-primary-blue_2">Subscribe</button>
                    </div>
                    <span class="text-color-light_brown_2">for only 5$ / month</span>
                </td>
            </tr>
            </tbody>
        </table>
    </div>

<div class="wrapper wrapper-border-bt mb-80">
    <h3 class="text-center pg-top-6 mb-42 text-title-2">Required fields in bug reports:</h3>
    <div class="mb-12 text-bold clearfix checkbox-custom-2 pg-top-6 text-size-13">
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm ">
                <div class="required-field">
                    <input type="checkbox" id="participant_2" checked="true">
                    <label for="participant_2" class="pull-left text-color-light_brown_2">
                            <span>Title:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_3" checked="true">
                    <label for="participant_3" class="pull-left text-color-light_brown_2">
                        <span>Description:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_4" checked="true">
                    <label for="participant_4" class="pull-left text-color-light_brown_2">
                        <span>Expected results:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_5" checked="true">
                    <label for="participant_5" class="pull-left text-color-light_brown_2">
                        <span>Steps to reproduce:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_6" checked="true">
                    <label for="participant_6" class="pull-left text-color-light_brown_2">
                        <span>Bug type:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_7" checked="true">
                    <label for="participant_7" class="pull-left text-color-light_brown_2">
                        <span>Reproducibility:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_8" checked="true">
                    <label for="participant_8" class="pull-left text-color-light_brown_2">
                        <span>Severity:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_9" checked="true">
                    <label for="participant_9" class="pull-left text-color-light_brown_2">
                        <span>Priority:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_10" checked="true">
                    <label for="participant_10" class="pull-left text-color-light_brown_2">
                        <span>Attachments:</span>
                    </label>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-xs-12 mb-28">
            <div class="required-fields margin-vertical-5-sm">
                <div class="required-field">
                    <input type="checkbox" id="participant_11" checked="true">
                    <label for="participant_11" class="pull-left text-color-light_brown_2">
                        <span>Device:</span>
                    </label>
                </div>
            </div>
        </div>

    </div>
</div>
<div class="wrapper-horizontal">
    <h3 class="text-center pg-top-6 mb-42 text-title-2">User permissions:</h3>
</div>
<div class="table-responsive">
    <table border="0" class="table table-bordered checkbox-custom-2 user-permissions text-size-13">
        <thead>
        <tr class="border-bottom border-top text-left">
            <th><span class="pg-16-left">Action:</span></th>
            <th class="text-left">Account manager:</th>
            <th>Project manager:</th>
            <th>Developer:</th>
            <th>Tester:</th>
            <th>Beta tester:</th>
            <th>Client:</th>
        </tr>
        </thead>

        <tbody>
        <tr>
            <td><b class="pg-16-left text-bold">Invite users</b></td>
            <td>
                <input type="checkbox" id="p_1" checked="true">
                <label for="p_1"></label>
            </td>
            <td>
                <input type="checkbox" id="p_2" checked="true">
                <label for="p_2"></label>
            </td>
            <td>
                <input type="checkbox" id="p_3" checked="true">
                <label for="p_3"></label></td>
            <td>
                <input type="checkbox" id="p_4" checked="true">
                <label for="p_4"></label></td>
            <td>
                <input type="checkbox" id="p_5" checked="true">
                <label for="p_5"></label></td>
            <td>
                <input type="checkbox" id="p_6" checked="true">
                <label for="p_6"></label></td>
            </td>
        </tr>
        <tr>
            <td><b class="pg-16-left text-bold">Create/edit a test plan</b></td>
            <td>
                <input type="checkbox" id="p_1" checked="true">
                <label for="p_1"></label>
            </td>
            <td>
                <input type="checkbox" id="p_2" checked="true">
                <label for="p_2"></label>
            </td>
            <td>
                <input type="checkbox" id="p_3" checked="true">
                <label for="p_3"></label></td>
            <td>
                <input type="checkbox" id="p_4" checked="true">
                <label for="p_4"></label></td>
            <td>
                <input type="checkbox" id="p_5" checked="true">
                <label for="p_5"></label></td>
            <td>
                <input type="checkbox" id="p_6">
                <label for="p_6"></label></td>
            </td>
        </tr>
        <tr>
            <td><b class="pg-16-left text-bold">Create projects</b></td>
            <td>
                <input type="checkbox" id="p_1" checked="true">
                <label for="p_1"></label>
            </td>
            <td>
                <input type="checkbox" id="p_2" checked="true">
                <label for="p_2"></label>
            </td>
            <td>
                <input type="checkbox" id="p_3" checked="true">
                <label for="p_3"></label></td>
            <td>
                <input type="checkbox" id="p_4" checked="true">
                <label for="p_4"></label></td>
            <td>
                <input type="checkbox" id="p_5" checked="true">
                <label for="p_5"></label></td>
            <td>
                <input type="checkbox" id="p_6">
                <label for="p_6"></label></td>
            </td>
        </tr>
        <tr>
            <td><b class="pg-16-left text-bold">Delete projects</b></td>
            <td>
                <input type="checkbox" id="p_1" checked="true">
                <label for="p_1"></label>
            </td>
            <td>
                <input type="checkbox" id="p_2" checked="true">
                <label for="p_2"></label>
            </td>
            <td>
                <input type="checkbox" id="p_3" checked="true">
                <label for="p_3"></label></td>
            <td>
                <input type="checkbox" id="p_4" checked="true">
                <label for="p_4"></label></td>
            <td>
                <input type="checkbox" id="p_5" checked="true">
                <label for="p_5"></label></td>
            <td>
                <input type="checkbox" id="p_6" checked="true">
                <label for="p_6"></label></td>
            </td>
        </tr>
        </tbody>

    </table>
</div>
