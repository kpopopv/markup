<?php

/* @var $this yii\web\View */

$this->title = 'Test plan create';
?>

<div class="wrapper">
    <div class="row">
        <div class="col-lg-5 col-md-12">
            <div class="text-title-2 text-bold">
                Create / edit a test plan - <span class="text-color-light_brown_2">“</span>QAOR Redesign<span class="text-color-light_brown_2">”</span>
            </div>
        </div>

        <div class="col-lg-7 col-md-12 col-sm-12">
            <nav class="navbar-page-nav">
                <ul class="navbar-page">
                    <li class="folding autosave margin-vertical-10-lg">
                        <span>Autosave:</span>
                        <div class="checkbox-move-wr">
                            <input type="checkbox" class="move" id="checkboxAutosave"><label for="checkboxAutosave"></label>
                        </div>
                    </li>
                    <li>
                        <a href="#" class="link link-light_brown_2" data-toggle="modal" data-target="#testModal">Import from CSV</a>
                    </li>
                    <li class="separator">l</li>
                    <li>
                        <a href="#" class="link link-light_brown_2" data-toggle="modal" data-target="#testModal">Export to CSV</a>
                    </li>
                    <li class="separator">l</li>
                    <li>
                        <a href="#" class="link link-light_brown_2">Expand / collapse all</a>
                    </li>
                </ul>
            </nav>
        </div>
    </div>
</div>

<div class="wrapper">
    <div class="row">
        <div class="col-md-12">

            <div class="panel-group" role="tablist" aria-multiselectable="true">
                <div class="panel-heading" role="tab"">
                    <div class="panel-title">
                        <div class="panel-darkly-brown panel-edit">
                            <ul class="panel-menu panel-menu_left responsive-menu block-center-sm">
                                <li class="block-center-sm margin-vertical-5-sm">
                                    <a href="javascript:void(0);" class="sprite sprite-move-white margin-horizontal-auto"></a>
                                </li>
                                <li class="block-center-sm margin-vertical-5-sm">
                                    <a role="button"
                                       data-toggle="collapse"
                                       data-parent="#accordion"
                                       href="#collapseRootOne"
                                       aria-expanded="true"
                                       class="text-bold pg-16 link link-white">Component #1:</a>
                                </li>
                                <li class="block-center-sm margin-vertical-5-sm">
                                    <div class="select select-primary">
                                        <select>
                                            <option disabled selected>ComboBox</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                        </select>
                                    </div>
                                </li>
                                <li class="block-center-sm margin-vertical-5-sm">
                                    <a href="#" class="link link-white">+ Add a new component</a>
                                </li>
                                <li></li> <!-- Hack resp -->
                            </ul>

                            <ul class="panel-menu panel-menu_right block-center-sm margin-vertical-5-sm">
                                <li class="pull-right float-none-sm">
                                    <a href="javascript:void(0);" class="link link-white">
                                        Expand / collapse all cases of this component
                                    </a>
                                </li>
                                <li></li> <!-- Hack resp -->
                            </ul>

                        </div>
                    </div>
                </div>

                <div id="collapseRootOne" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">

                        <!--  Case 1 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-edit panel-child">

                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildOne"
                                           class="sprite sprite-collapse-grey pull-right"
                                           aria-expanded="true">
                                        </a>

                                        <ul class="panel-menu panel-menu_left">
                                            <li>
                                                <a href="javascript:void(0);" class="sprite sprite-move"></a>
                                            </li>
                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildOne"
                                                   aria-expanded="false"
                                                   class="text-bold text-color-violet pg-16 link">Case #1:</a>
                                                <span class="text-italic">Insert action here...</span>
                                            </li>
                                            <li></li> <!-- Hack resp -->
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <div id="collapseChildOne" class="panel-collapse collapse in" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <section>
                                        <header>
                                            <div class="text-bold text-center hr pb-16">Optional fields:</div>
                                        </header>

                                        <div class="row">
                                            <div class="col-lg-8 mb-22">
                                                <div class="row">
                                                    <div class="col-lg-6 mb-22">
                                                        <section>
                                                            <header class="text-bold">Preconditions:</header>

                                                            <div>
                                                                <textarea class="for-form" placeholder="Lorem impsum dolor..."></textarea>
                                                            </div>

                                                        </section>
                                                    </div>
                                                    <div class="col-lg-6 mb-22">
                                                        <section>
                                                            <header class="text-bold">Expected result:</header>

                                                            <div>
                                                                <textarea class="for-form" placeholder="Lorem impsum dolor..."></textarea>
                                                            </div>
                                                        </section>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <ul class="panel-menu panel-menu_left responsive-menu">
                                                            <li>
                                                                <span class="text-bold pg-16">Priority:</span>
                                                            </li>
                                                            <li>
                                                                <div class="select select-primary">
                                                                    <select>
                                                                        <option disabled selected>None selected</option>
                                                                        <option value="1">One</option>
                                                                        <option value="2">Two</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="drag-and-drop sm visible-lg hidden-md hidden-sm hidden-xs">
                                                                    <span class="drag-and-drop_text">Drag and drop an image to be used as reference with this test case</span>
                                                                </div>
                                                            </li>
                                                            <li></li>
                                                        </ul>
                                                        <div class="mb-22"></div>
                                                        <div class="drag-and-drop sm hidden-lg visible-md visible-sm visible-xs mb-22">
                                                            <span class="drag-and-drop_text">Drag and drop an image to be used as reference with this test case</span>
                                                        </div>

                                                        <input type="checkbox" id="checkboxForce1" />
                                                        <label for="checkboxForce1" class="text-color-light_brown_2">Force respondents to submit a screenshot or a video when they complete this test case.</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <section>
                                                    <header class="text-bold">Steps to reproduce:</header>

                                                    <div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">1.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">2.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">3.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue">
                                                            <a href="javascript:void(0);" class="link link-white">
                                                                <span class="sprite sprite-plus-white"></span><span class="text-bold pg-16"></span>Add a step
                                                            </a>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                        </div>
                        <!-- End  case 1 -->

                        <!--  Case 2 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="false">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-edit panel-child">

                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildTwo"
                                           class="sprite sprite-collapse-grey pull-right"
                                           aria-expanded="true">
                                        </a>

                                        <ul class="panel-menu panel-menu_left">
                                            <li>
                                                <a href="javascript:void(0);" class="sprite sprite-move"></a>
                                            </li>
                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildTwo"
                                                   aria-expanded="false"
                                                   class="text-bold text-color-violet pg-16 link">Case #2:</a>
                                                <span class="text-italic">Insert action here...</span>
                                            </li>
                                            <li></li> <!-- Hack resp -->
                                        </ul>

                                    </div>
                                </div>
                            </div>

                            <div id="collapseChildTwo" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <section>
                                        <header>
                                            <div class="text-bold text-center hr pb-16">Optional fields:</div>
                                        </header>

                                        <div class="row">
                                            <div class="col-lg-8 mb-22">
                                                <div class="row">
                                                    <div class="col-lg-6 mb-22">
                                                        <section>
                                                            <header class="text-bold">Preconditions:</header>

                                                            <div>
                                                                <textarea class="for-form" placeholder="Lorem impsum dolor..."></textarea>
                                                            </div>

                                                        </section>
                                                    </div>
                                                    <div class="col-lg-6 mb-22">
                                                        <section>
                                                            <header class="text-bold">Expected result:</header>

                                                            <div>
                                                                <textarea class="for-form" placeholder="Lorem impsum dolor..."></textarea>
                                                            </div>
                                                        </section>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <ul class="panel-menu panel-menu_left responsive-menu">
                                                            <li>
                                                                <span class="text-bold pg-16">Priority:</span>
                                                            </li>
                                                            <li>
                                                                <div class="select select-primary">
                                                                    <select>
                                                                        <option disabled selected>None selected</option>
                                                                        <option value="1">One</option>
                                                                        <option value="2">Two</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="drag-and-drop sm visible-lg hidden-md hidden-sm hidden-xs">
                                                                    <span class="drag-and-drop_text">Drag and drop an image to be used as reference with this test case</span>
                                                                </div>
                                                            </li>
                                                            <li></li>
                                                        </ul>
                                                        <div class="mb-22"></div>
                                                        <div class="drag-and-drop sm hidden-lg visible-md visible-sm visible-xs mb-22">
                                                            <span class="drag-and-drop_text">Drag and drop an image to be used as reference with this test case</span>
                                                        </div>

                                                        <input type="checkbox" id="checkboxForce2" />
                                                        <label for="checkboxForce2" class="text-color-light_brown_2">Force respondents to submit a screenshot or a video when they complete this test case.</label>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-4">
                                                <section>
                                                    <header class="text-bold">Steps to reproduce:</header>

                                                    <div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">1.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">2.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue"><span class="text-bold pg-16">3.</span>Lorem impsum dolor...</div>
                                                        <div class="panel-gradient-blue">
                                                            <a href="javascript:void(0);" class="link link-white">
                                                                <span class="sprite sprite-plus-white"></span><span class="text-bold pg-16"></span>Add a step
                                                            </a>
                                                        </div>
                                                    </div>
                                                </section>
                                            </div>
                                        </div>

                                    </section>
                                </div>
                            </div>
                        </div>
                        <!-- End  case 2 -->


                    </div>
                </div>

            </div>

        </div>
    </div>
</div>


<div class="modal fade" id="testModal" tabindex="-1" role="dialog" aria-labelledby="testModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close pull-right sprite sprite-close" data-dismiss="modal" aria-label="Close"></button>
                <div class="text-bold modal-title">
                    Import from CSV
                </div>
            </div>
            <div class="hr"></div>

            <div class="modal-body">

                <section class="mb-45">
                    <header class="text-title-2 mb-45">
                        Mandatory CSV importing guidelines
                    </header>

                    <ul class="list">
                        <li>You can download out CSV template file and adapt it to your needs</li>
                        <li>Mandatory columns: <span class="text-bold">Component, User Action</span> </li>
                        <li>CSV filed delimited has to be a comma (,)</li>
                        <li>CSV string delimited has to be a double quotation mark (")</li>
                    </ul>
                </section>

                <section class="mb-80">
                    <header class="text-title-2 mb-45">
                        Mandatory CSV importing guidelines
                    </header>

                    <ul class="list">
                        <li>You can download out CSV template file and adapt it to your needs</li>
                        <li>Mandatory columns: <span class="text-bold">Component, User Action</span> </li>
                        <li>CSV filed delimited has to be a comma (,)</li>
                        <li>CSV string delimited has to be a double quotation mark (")</li>
                    </ul>
                </section>

                <div class="text-center">
                    <a href="#" class="link link-blue-1 text-title-2">Attach a CSV file</a>
                </div>

            </div>

            <div class="modal-footer">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-darkly_white" data-dismiss="modal">Cancel</button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue">Import</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>