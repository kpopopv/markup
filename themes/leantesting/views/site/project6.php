<?php

/* @var $this yii\web\View */

$this->title = 'Bug tracker';
?>

<div class="project6 clearfix">

        <div class="col-lg-3 col-md-4 col-sm-12 hidden-xs">
            <div class="row">
                <div class="bug-tracker-bar">
                    <div class="search-panel wrapper-full clearfix">
                        <div class="toolbar-panel">
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="toolbar-panel-cell block-center-sm margin-vertical-5-sm">
                                    <div class="row">
                                        <div class="form-search">
                                            <input class="for-form bordered-light width-120 text-size-9" placeholder="Search all bugs..." type="text"/>
                                            <i class="sprite-project sprite-project-track-bar-search_1"> </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5 col-md-5 col-sm-12">
                                <div class="toolbar-panel-cell block-center-sm pg-16-left-md margin-vertical-5-sm">
                                    <div class="row">
                                        <div class="form-search">
                                            <input class="for-form bordered-light width-120 text-size-9" placeholder="Filter by tags..." type="text"/>
                                            <i class="sprite-project sprite-project-track-bar-search_2"> </i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2 col-sm-12 block-center-sm">
                                <div class="toolbar-panel-cell">
                                        <i class="sprite-project sprite-project-collapse" id="btn-hide-bug-tracker"></i>
                                    </div>
                            </div>
                        </div>
                    </div>
                    <div class="filters-panel wrapper-full clearfix hr">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell no-margin-right col-lg-2 vertical-align-top">
                                <div class="block-center-sm margin-vertical-5-sm">
                                    <label class="text-size-11">Filter&nbsp;by:</label>
                                </div>
                            </div>
                            <div class="toolbar-panel-cell no-margin-right col-lg-10">
                                    <div class="col-lg-12 clearfix">
                                        <div class="row bug-tracker-select-box">
                                        <div class="col-lg-3 col-md-3 col-sm-12">
                                            <div class="row">
                                                <div class="select select-primary text-size-9 table-panel-vertical-middle">
                                                    <select>
                                                        <option value="1">Version</option>
                                                        <option value="2">Version</option>
                                                        <option value="3">Version</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 block-center-sm">
                                            <div class="row">
                                                <div class="select select-primary text-size-9 table-panel-vertical-middle">
                                                    <select>
                                                        <option value="1">Created</option>
                                                        <option value="2">Created</option>
                                                        <option value="3">Created</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 block-center-sm">
                                            <div class="row">
                                                <div class="select select-primary text-size-9 table-panel-vertical-middle">
                                                    <select>
                                                        <option value="1">Updated</option>
                                                        <option value="2">Updated</option>
                                                        <option value="3">Updated</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-3 col-sm-12 block-center-sm">
                                            <div class="row">

                                                    <button class="btn panel-darkly-white table-panel-vertical-middle block-center-sm vertical-align-middle" style="margin-left:4px;">
                                                        <div class="table-panel">
                                                            <div class="table-panel-cell">
                                                                <i class="sprite-project sprite-project-options"></i>
                                                            </div>
                                                            <div class="table-panel-cell table-panel-vertical-middle pg-left-6">
                                                                <span>Options</span>
                                                            </div>

                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        </div>
                    <div class="messages-panel margin-vertical-5-sm text-size-13">
                        <div class="message-item message-item-color-lime">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                        <input type="checkbox" id="test1">
                                        <label for="test1" class="text-color-light_brown_2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">1</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                        <div class="toolbar-panel-cell">
                                            <i class="sprite-project sprite-project-msg-icon-1-dots"></i>
                                        </div>
                                        <div class="toolbar-panel-cell">
                                            <i class="sprite-project sprite-project-msg-icon-1-user"></i>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-blue">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                        <input type="checkbox" id="test2">
                                        <label for="test2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">2</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-2-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-2-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-purple">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                    <input type="checkbox" id="test3">
                                        <label for="test3">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">3</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-3-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-3-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-pink">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                    <input type="checkbox" id="test4">
                                    <label for="test4" class="text-color-light_brown_2">
                                        <div class="toolbar-panel clearfix">
                                            <div class="toolbar-panel-cell">4</div>
                                            <div class="toolbar-panel-cell message-item-text">
                                                Not enough space is present between the two last
                                                paragraphs in landscape orientation.
                                            </div>
                                        </div>
                                    </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                    <div class="toolbar-panel messages-icons">
                                        <div class="toolbar-panel-cell">
                                            <i class="sprite-project sprite-project-msg-icon-4-dots"></i>
                                        </div>
                                        <div class="toolbar-panel-cell">
                                            <i class="sprite-project sprite-project-msg-icon-4-user"></i>
                                        </div>
                                    </div>
                                        </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-emerald">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                        <input type="checkbox" id="test5">
                                        <label for="test5" class="text-color-light_brown_2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">5</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-5-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-5-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-green-light">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                        <input type="checkbox" id="test6">
                                        <label for="test6" class="text-color-light_brown_2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">6</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-6-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-6-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-lime-light">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                        <input type="checkbox" id="test7">
                                        <label for="test7" class="text-color-light_brown_2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">7</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-7-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-7-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="message-item message-item-color-red">
                            <div class="toolbar-panel clearfix">
                                <div class="toolbar-panel-cell col-md-9 col-lg-10 col-sm-9">
                                    <div class="row">
                                    <input type="checkbox" id="test9">
                                        <label for="test9" class="text-color-light_brown_2">
                                            <div class="toolbar-panel clearfix">
                                                <div class="toolbar-panel-cell">8</div>
                                                <div class="toolbar-panel-cell message-item-text">
                                                    Not enough space is present between the two last
                                                    paragraphs in landscape orientation.
                                                </div>
                                            </div>
                                        </label>
                                    </div>
                                </div>
                                <div class="toolbar-panel-cell col-md-3 col-lg-2 col-sm-3">
                                    <div class="row">
                                        <div class="toolbar-panel messages-icons">
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-8-dots"></i>
                                            </div>
                                            <div class="toolbar-panel-cell">
                                                <i class="sprite-project sprite-project-msg-icon-8-user"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            </div>

        <div class="col-lg-8 col-md-8 col-xs-12 content-wrapper">
            <section class="wrapper">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12">
                        <div class="block-center-sm">
                            <h2 class="no-margin pg-16-left toolbar-panel-height-27 margin-vertical-5-sm">#1 Not enough space is present between the two last paragraphs in landscape orientation.</h2>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12">
                        <div class="block-center-sm text-right">
                            <button class="btn btn-primary-blue text-size-13">Report a new bug</button>
                        </div>
                    </div>
                </div>
            </section>
            <div class="content clearfix">
                    <div class="block block-top row bug-tracker-content">
                        <div class="col-lg-9 col-md-8">
                            <div class="wrapper pg-left-6">
                                    <h3 class="text-icon text-icon-right no-margin mb-28 text-title-2">Description:
                                        <i class="sprite-project sprite-project-edit-gray"></i>
                                    </h3>
                                    <p class="text-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim imperdiet magna lacinia pretium. Sed purus odio, feu-
                                        giat sit amet rutrum nec, luctus id enim. Integer tristique eros ut augue egestas, ac euismod diam pretium. Aliquam vel auctor
                                        nulla, non pulvinar urna. Nunc et consectetur justo, eu blandit lorem. Ut non dui quam. Sed gravida magna id dolor elementum.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim imperdiet magna lacinia pretium. Sed purus odio, feu-
                                        giat sit amet rutrum nec, luctus id enim. Integer tristique eros ut augue egestas, ac euismod diam pretium. </p>
                                    <h3 class="row-hor text-title-2">Expected behaviour:</h3>
                                    <p class="text-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim imperdiet magna lacinia pretium. Sed purus odio, feu-
                                        giat sit amet rutrum nec, luctus id enim. Integer tristique eros ut augue egestas, ac euismod diam pretium. Aliquam vel auctor
                                        nulla, non pulvinar urna. Nunc et consectetur justo, eu blandit lorem. Ut non dui quam. Sed gravida magna id dolor elementum.
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam dignissim imperdiet magna lacinia pretium. Sed purus odio, feu-
                                        giat sit amet rutrum nec, luctus id enim. Integer tristique eros ut augue egestas, ac euismod diam pretium. </p>
                            </div>
                            <div class="text-border-bt-light mb-40"></div>
                            <div class="wrapper pg-left-6">
                                <h3 class="text-icon text-icon-right mb-40 text-title-2">Steps to reproduce:
                                    <i class="sprite-project sprite-project-edit-gray"></i>
                                </h3>
                                <div class="steps">
                                    <div class="step-item mb-22 media">
                                        <div class="text-center media-left">
                                            <div class="step-number">1</div>
                                        </div>
                                        <div class="step-content media-body">Access the page http://qaonrequest.stgng.com/open-device-lab/ .</div>
                                    </div>
                                    <div class="step-item mb-22 media">
                                        <div class="text-center media-left">
                                            <div class="step-number">2</div>
                                        </div>
                                        <div class="step-content media-body">Access the page http://qaonrequest.stgng.com/open-device-lab/ .</div>
                                    </div>
                                    <div class="step-item mb-22 media">
                                        <div class="text-center media-left">
                                            <div class="step-number">3</div>
                                        </div>
                                        <div class="step-content media-body">Access the page http://qaonrequest.stgng.com/open-device-lab/ .</div>
                                    </div>
                                    <div class="step-item mb-22 media">
                                        <div class="text-center media-left">
                                            <div class="step-number">4</div>
                                        </div>
                                        <div class="step-content media-body">Access the page http://qaonrequest.stgng.com/open-device-lab/ .</div>
                                    </div>
                                    <div class="step-item mb-22 media">
                                        <div class="text-center media-left">
                                            <div class="step-number">5</div>
                                        </div>
                                        <div class="step-content media-body">Access the page http://qaonrequest.stgng.com/open-device-lab/ .</div>
                                    </div>
                                </div>
                            </div>
                            <div class="text-border-bt-light mb-40"></div>
                            <div class="wrapper pg-left-6">
                                <h3 class="mb-40 text-title-2">Attachments:</h3>
                                <div class="row">
                                    <div class="attachments">
                                       <div class="col-lg-3 col-md-6 col-sm-3">
                                           <div class="attachment-item margin-vertical-5-sm">
                                               <div class="attachment-content"></div>
                                               <i class="attachment-close pull-right sprite sprite-close"></i>
                                           </div>
                                       </div>
                                        <div class="col-lg-3 col-md-6 col-sm-3">
                                            <div class="attachment-item margin-vertical-5-sm">
                                                <div class="attachment-content"></div>
                                                <i class="attachment-close pull-right sprite sprite-close"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-3">
                                            <div class="attachment-item margin-vertical-5-sm">
                                                <div class="attachment-content"></div>
                                                <i class="attachment-close pull-right sprite sprite-close"></i>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col-md-6 col-sm-3">
                                            <div class="attachment-new">
                                                <div class="attachment-content text-center row-hor">
                                                    <div class="sprite-project sprite-project-attachment-new mb-12"></div>
                                                    <div>
                                                        <a href="javascript:void(0);" class="link link-blue" >Drag a file here to attach</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="text-border-bt-light mb-40"></div>
                            <div class="wrapper pg-left-6">
                                <h3 class="mb-40 text-title-2">Comments:</h3>
                                 <div class="comments">
                                     <div class="row">
                                        <div class="comment-add-form clearfix">
                                            <div class="col-lg-11 col-md-10 col-sm-11 col-xs-10">
                                                <div class="input-container">
                                                    <input class="for-form placeholder-text-bold text-size-13" placeholder="Write a comment..."/>
                                                    <i class="sprite-project sprite-project-comment-user"></i>
                                                </div>
                                            </div>
                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                                <div class="row">
                                                    <button class="btn btn-primary-blue">Post</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                <div class="comments-list">
                                    <div class="comments-item mb-22">
                                        <div class="wrapper-full">
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="/themes/leantesting/images/filter-pacient-cap.png" width="38">
                                                </div>
                                                <div class="media-body text-size-13">
                                                    <div class="list-info-inline">
                                                        <ul>
                                                            <li><b>Pawel Utr</b></li>
                                                            <li>tester</li>
                                                            <li>2015-12-16 20:14:18</li>
                                                        </ul>
                                                    </div>
                                                    <div class="comment-content">
                                                        <p class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus sem nec nibh molestie, eget
                                                            egestas dui facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus malesuada
                                                            blandit ante, tempor ullamcorper quam feugiat et.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="comments-item">
                                        <div class="wrapper-full">
                                            <div class="media">
                                                <div class="media-left">
                                                    <img src="/themes/leantesting/images/filter-pacient-cap.png" width="38">
                                                </div>
                                                <div class="media-body text-size-13">
                                                    <div class="list-info-inline">
                                                        <ul>
                                                            <li><b>Pawel Utr</b></li>
                                                            <li>tester</li>
                                                            <li>2015-12-16 20:14:18</li>
                                                        </ul>
                                                    </div>
                                                    <div class="comment-content">
                                                        <p class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam faucibus sem nec nibh molestie, eget
                                                            egestas dui facilisis. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus malesuada
                                                            blandit ante, tempor ullamcorper quam feugiat et.
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 </div>
                            </div>
                            <div class="text-border-bt-light mb-40"></div>
                            <div class="wrapper pg-left-6">
                                <h3 class="mb-40 text-title-2">Recent activity:</h3>
                                <div class="activities">
                                    <div class="activity-item mb-22">
                                        <div class="wrapper-full">
                                        <div class="activity-item-header clearfix">
                                            <i class="sprite-project sprite-project-activity-bug pull-left "></i>
                                            <div class="list-info-inline">
                                                <ul class="no-margin">
                                                    <li>Friday, 18-Dec-2015 21:10:00 UTC</li>
                                                    <li class="text-color-violet"><b>Lorem Ipsum</b></li>
                                                    <li><a href="#" class="link link-blue">edited the bug</a></li>
                                                    <li class="text-italic-natural">field: description</li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="activity-item-content clearfix text-size-13">
                                            <div class="col-lg-6">
                                                <h4>Original value:</h4>
                                                <p class="text-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Etiam faucibus sem nec nibh molestie, eget egestas dui
                                                    facilisis. Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit. Vivamus malesuada blandit ante, tempor
                                                    ullamcorper quam feugiat et.
                                                </p>
                                            </div>
                                            <div class="col-lg-6">
                                                <h4>New value:</h4>
                                                <p class="text-description">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                                    Etiam faucibus sem nec nibh molestie, eget egestas dui
                                                    facilisis. Lorem ipsum dolor sit amet, consectetur
                                                    adipiscing elit. Vivamus malesuada blandit ante, tempor
                                                    ullamcorper quam feugiat et.
                                                </p>
                                            </div>
                                        </div>
                                        </div>
                                    </div>
                                    <div class="activity-item mb-22">
                                        <div class="wrapper-full">
                                            <div class="activity-item-header clearfix">
                                                <i class="sprite-project sprite-project-activity-image pull-left"></i>
                                                <div class="list-info-inline">
                                                    <ul class="no-margin">
                                                        <li>Friday, 18-Dec-2015 21:10:00 UTC</li>
                                                        <li class="text-color-violet"><b>Lorem Ipsum</b></li>
                                                        <li><a href="#" class="link link-blue">edited the bug</a></li>
                                                        <li class="text-italic-natural">field: description</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="activity-item mb-22">
                                        <div class="wrapper-full">
                                            <div class="activity-item-header clearfix">
                                                <i class="sprite-project sprite-project-activity-comment pull-left"></i>
                                                <div class="list-info-inline">
                                                    <ul class="no-margin">
                                                        <li>Friday, 18-Dec-2015 21:10:00 UTC</li>
                                                        <li class="text-color-violet"><b>Lorem Ipsum</b></li>
                                                        <li><a href="#" class="link link-blue">edited the bug</a></li>
                                                        <li class="text-italic-natural">field: description</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="col-lg-3 col-md-4">
                    <div class="right-bar">
                        <div class="wrapper">
                            <h3 class="text-icon text-icon-right no-margin mb-28 text-title-2">Bug details:
                                <i class="sprite-project sprite-project-edit-gray"></i>
                            </h3>
                            <div>
                               <table class="table no-border bug-details">
                                   <tr>
                                       <td><b>Found in version:</b></td>
                                       <td>10/12/2015</td>
                                   </tr>
                                   <tr>
                                       <td><b>Project section:</b></td>
                                       <td>Open Device Lab</td>
                                   </tr>
                                   <tr>
                                       <td><b>Type:</b></td>
                                       <td>Visual</td>
                                   </tr>
                                   <tr>
                                       <td><b>Created:</b></td>
                                       <td>2016-03-10<br/>
                                           17:08:55 UTC</td>
                                   </tr>
                                   <tr>
                                       <td><b>Reporter:	</b></td>
                                       <td>Petyr Baelish</td>
                                   </tr>
                                   <tr>
                                       <td><b>Severity:</b></td>
                                       <td>Mino</td>
                                   </tr>
                                   <tr>
                                       <td><b>Reproducibility:</b></td>
                                       <td>Always</td>
                                   </tr>
                               </table>
                            </div>
                        </div>
                        <div class="text-border-bt-light mb-40"></div>
                        <div class="wrapper">
                            <h3 class="text-icon text-icon-right no-margin mb-28 text-title-2">Tags:
                                <i class="sprite-project sprite-project-edit-gray"></i>
                            </h3>
                            <div class="tags text-color-light_brown_2">
                                <a class="active">#ExampleTag1</a>
                                <a>#QAOR </a>
                                <a>#GalaxyS6</a>
                                <a>#TermsAndConditions</a>
                                <a>#Critical</a>
                            </div>
                        </div>
                        <div class="text-border-bt-light mb-40"></div>
                        <div class="wrapper">
                                <h3 class="text-icon text-icon-right no-margin mb-28 text-title-2 mb-36">Change status / assign:
                                    <i class="sprite-project sprite-project-edit-gray"></i>
                                </h3>
                                <div class="row mb-12">
                                    <div class="col-lg-2 col-md-12 col-sm-2 col-xs-2 toolbar-panel-height-35 block-center-md block-center-1280 block-offset-none-1280">
                                        <b>Status:</b>
                                    </div>
                                    <div class="col-lg-8 col-lg-offset-2 col-md-12 col-md-offset-0 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 block-center-1280 block-offset-none-1280">
                                        <div class="select select-primary">
                                            <select>
                                                <option value="1">Open</option>
                                                <option value="2">Closed</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="row mb-12">
                                    <div class="col-lg-2 col-md-12 col-sm-2 col-xs-2 toolbar-panel-height-35 block-center-md block-center-1280 block-offset-none-1280">
                                        <b>Assign&nbsp;to:</b>
                                    </div>
                                    <div class="col-lg-8 col-lg-offset-2 col-md-12 col-md-offset-0 col-sm-offset-2 col-sm-8 col-xs-offset-2 col-xs-8 block-center-1280 block-offset-none-1280">
                                        <div class="select select-primary">
                                            <select>
                                                <option value="1">Lorem ipsum</option>
                                                <option value="2">Lorem ipsum</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                        </div>
                        <div class="text-border-bt-light mb-40"></div>
                        <div class="wrapper">
                            <div class="mb-22">
                                <button class="btn btn-lg-w-100 btn-primary-darkly-white">Clone bug</button>
                            </div>
                            <div class="mb-22">
                                <button class="btn btn-lg-w-100 btn-primary-darkly-white">Move bug</button>
                            </div>
                            <button class="btn btn-lg-w-100 btn-primary-darkly-white">Delete bug</button>
                        </div>
                        <div class="text-border-bt-light mb-40"></div>
                        <div class="wrapper">
                            <h3 class="text-icon text-icon-right no-margin mb-28 text-title-2">Platform details:
                                <i class="sprite-project sprite-project-edit-gray"></i>
                            </h3>
                            <div class="table-responsive">
                                <table class="table no-border">
                                    <tr>
                                        <td><b>Device type</b></td>
                                        <td>Windows PC</td>
                                    </tr>
                                    <tr>
                                        <td><b>Model</b></td>
                                        <td>Custom/Other</td>
                                    </tr>
                                    <tr>
                                        <td><b>OS</b></td>
                                        <td>Windows 8</td>
                                    </tr>
                                    <tr>
                                        <td><b>Browser</b></td>
                                        <td>Chrome Latest</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
</div>