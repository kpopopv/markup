<?php

/* @var $this yii\web\View */

$this->title = 'Conversation view';
?>

<section class="wrapper text-center">
    <h2 class="no-margin">"QA on Request Redesign" conversation:</h2>
</section>
<div class="wrapper">
    <div class="col-lg-8">
        <div class="row">
            <div class="comments text-color-light_brown_2 text-size-13">
                <div class="comment-add-form">
                    <div class="col-lg-2">
                        <div class="comment-info">
                            <div class="wrapper">
                                <div class="avatar-wrapper">
                                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="textarea-container">
                            <textarea placeholder="Compose a message..."></textarea>
                            <div class="form-future">
                                <i class="sprite-project sprite-project-sharp"></i>
                                <a href="#" class="link link-blue-1 text-bold">Add tags...</a>
                            </div>
                        </div>
                        <div class="form-future mb-28">
                            <i class="sprite-project sprite-project-plus"></i>
                            <a href="#" class="link link-blue-1 text-bold">Attach a file</a>
                            <button class="pull-right btn btn-small btn-primary-blue">Post</button>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="comment-item mb-28">
                    <div class="col-lg-2">
                        <div class="comment-info">
                            <div class="wrapper">
                                <div class="avatar-wrapper">
                                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                                </div>
                                <div class="text-left text-semibold block-center-xs">
                                    <p class="name text-size-12 no-margin">Lorem</p>
                                    <p class="family text-size-12 no-margin">Ipsum</p>
                                    <span class="date text-size-11">18/12/2015</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="comment-message">
                            <p class="no-margin text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id tortor nunc. Integer est lectus,
                                mollis ut imperdiet ac, elementum vel lorem. Donec a suscipit lectus. Suspendisse ut libero nec
                                mauris convallis laoreet. Cras eget turpis et nisi suscipit sollicitudin. Nunc ut ante arcu.
                            </p>
                            <div class="hr"></div>
                            <p class="no-margin comment-tags">Tags:
                                <span>#qaor</span>
                                <span>#redesign </span>
                                <span>#exampletag</span>
                                <i class="sprite-project sprite-project-pin pull-right float-none-xs"></i>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="comment-item mb-28">
                    <div class="col-lg-2">
                        <div class="comment-info">
                            <div class="wrapper">
                                <div class="avatar-wrapper">
                                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                                </div>
                                <div class="text-left text-semibold block-center-xs">
                                    <p class="name text-size-12 no-margin">Lorem</p>
                                    <p class="family text-size-12 no-margin">Ipsum</p>
                                    <span class="date text-size-11">18/12/2015</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="comment-message">
                            <p class="no-margin text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id tortor nunc. Integer est lectus,
                                mollis ut imperdiet ac, elementum vel lorem. Donec a suscipit lectus. Suspendisse ut libero nec
                                mauris convallis laoreet. Cras eget turpis et nisi suscipit sollicitudin. Nunc ut ante arcu.
                            </p>
                            <div class="hr"></div>
                            <p class="no-margin comment-tags">Tags:
                                <span>#qaor</span>
                                <span>#redesign </span>
                                <span>#exampletag</span>
                                <i class="sprite-project sprite-project-pin-gray pull-right float-none-xs"></i>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="comment-item mb-28">
                    <div class="col-lg-2">
                        <div class="comment-info">
                            <div class="wrapper">
                                <div class="avatar-wrapper">
                                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                                </div>
                                <div class="text-left text-semibold block-center-xs">
                                    <p class="name text-size-12 no-margin">Lorem</p>
                                    <p class="family text-size-12 no-margin">Ipsum</p>
                                    <span class="date text-size-11">18/12/2015</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-10">
                        <div class="comment-message">
                            <p class="no-margin text-medium">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque id tortor nunc. Integer est lectus,
                                mollis ut imperdiet ac, elementum vel lorem. Donec a suscipit lectus. Suspendisse ut libero nec
                                mauris convallis laoreet. Cras eget turpis et nisi suscipit sollicitudin. Nunc ut ante arcu.
                            </p>
                            <div class="comment-attachments toolbar-panel">
                                <div class="toolbar-panel-cell block-center-xs block-center-1280 margin-vertical-5-1280 block-center-sm margin-vertical-5-sm">Attachments:</div>
                                <div class="toolbar-panel-cell block-center-xs block-center-1280 margin-vertical-5-1280 block-center-sm margin-vertical-5-sm">
                                    <img src="/themes/leantesting/images/comment-attachment-cap.png"/>
                                </div>
                                <div class="toolbar-panel-cell block-center-xs block-center-1280 margin-vertical-5-1280 block-center-sm margin-vertical-5-sm">
                                    <img src="/themes/leantesting/images/comment-attachment-cap.png"/>
                                </div>
                                <div class="toolbar-panel-cell block-center-xs block-center-1280 margin-vertical-5-1280 block-center-sm margin-vertical-5-sm">
                                    <img src="/themes/leantesting/images/comment-attachment-cap.png"/>
                                </div>
                            </div>
                            <div class="hr"></div>
                            <p class="no-margin comment-tags">Tags:
                                <span>#qaor</span>
                                <span>#redesign </span>
                                <span>#exampletag</span>
                                <i class="sprite-project sprite-project-pin-gray pull-right float-none-xs"></i>
                            </p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="panel-filters">
            <div class="form-search text-size-13">
                <input class="for-form placeholder-text-italic text-italic-natural search" placeholder="Find keywords..."/>
                <i class='sprite-project sprite-project-search'></i>
            </div>
            <h3 class="text-size-15 wrapper-full no-margin">
                Filter by tags:
            </h3>
            <div class="tags text-color-light_brown_2 text-size-12">
                <a class="active">#ExampleTag1</a>
                <a>#QAOR </a>
                <a>#GalaxyS6</a>
                <a>#TermsAndConditions</a>
                <a>#Critical</a>
            </div>
            <h3 class="text-size-15 wrapper-full no-margin">
                Filter by participant:
            </h3>
            <div class="filter-participant checkbox-custom-2">
                <div class="filter-item">
                    <input type="checkbox" id="participant_1">
                    <label for="participant_1" class="pull-left">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell">
                                <img src="/themes/leantesting/images/filter-pacient-cap.png"/>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span>Lorem Ipsum</span>
                            </div>
                        </div>
                    </label>
                    <div class="clearfix"></div>
                </div>
                <div class="filter-item active">
                    <input type="checkbox" id="participant_2" checked="true">
                    <label for="participant_2" class="pull-left">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell">
                                <img src="/themes/leantesting/images/filter-pacient-cap.png"/>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span>Lorem Ipsum</span>
                            </div>
                        </div>
                    </label>
                    <div class="clearfix"></div>
                </div>
                <div class="filter-item">
                    <input type="checkbox" id="participant_3">
                    <label for="participant_3" class="pull-left">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell">
                                <img src="/themes/leantesting/images/filter-pacient-cap.png"/>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span>Lorem Ipsum</span>
                            </div>
                        </div>
                    </label>
                    <div class="clearfix"></div>
                </div>
                <div class="filter-item">
                    <input type="checkbox" id="participant_4">
                    <label for="participant_4" class="pull-left">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell">
                                <img src="/themes/leantesting/images/filter-pacient-cap.png"/>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span>Lorem Ipsum</span>
                            </div>
                        </div>
                    </label>
                    <div class="clearfix"></div>
                </div>
                <div class="filter-item">
                    <input type="checkbox" id="participant_5">
                    <label for="participant_5" class="pull-left">
                        <div class="toolbar-panel">
                            <div class="toolbar-panel-cell">
                                <img src="/themes/leantesting/images/filter-pacient-cap.png"/>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span>Lorem Ipsum</span>
                            </div>
                        </div>
                    </label>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>


