<?php

/* @var $this yii\web\View */

$this->title = 'User settings';
?>

<div class="project5">
<div class="row">
    <div class="col-lg-4">
        <div class="content">
            <div class="sidebar">
                <div class="wrapper text-border-bt-light">
                    <div class="toolbar-panel text-center">
                        <div class="toolbar-panel-cell">
                            <button class="btn btn-primary-darkly-white text-size-13">Export</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button class="btn btn-primary-darkly-white text-size-13">Archive</button>
                        </div>
                        <div class="toolbar-panel-cell">
                            <button class="btn btn-primary-red text-size-13">Delete</button>
                        </div>
                    </div>
                </div>
                <div class="wrapper-full settings-panel">
                    <h3 class="no-margin mb-40 text-size-15">Project versions:</h3>
                    <div class="row">
                        <div class="input-row">
                            <div class="col-lg-10">
                                <input type="text" class="for-form placeholder-text-italic text-italic-natural text-size-13" placeholder="e.g. v1.0.19" />
                            </div>
                            <div class="col-lg-2">
                                <div class="block-center-lg margin-vertical-5-lg">
                                    <div class="row">
                                        <button class="btn panel-darkly-white">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive">
                    <table border="0" class="table inverse-stripes no-border active-icons text-size-13">
                        <tbody>
                            <tr>
                                <td width="80%">1.0.2 #6</td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-edit-row"></i>
                                </td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-delete-row"></i>
                                </td>
                            </tr>
                            <tr>
                                <td width="80%">1.0.3</td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-edit-row"></i>
                                </td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-delete-row"></i>
                                </td>
                            </tr>
                            <tr>
                                <td width="80%">1.0.4</td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-edit-row"></i>
                                </td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-delete-row"></i>
                                </td>
                            </tr>
                            <tr>
                                <td width="80%">1.0.5 #5</td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-edit-row"></i>
                                </td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-delete-row"></i>
                                </td>
                            </tr>
                            <tr>
                                <td width="80%">1.0.6 #3</td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-edit-row"></i>
                                </td>
                                <td class="text-center text-bold">
                                    <i class="sprite-project sprite-project-delete-row"></i>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="wrapper-full text-border-bt-light">
                    <ul class="pagination text-color-light_brown_2">
                        <li class="hidden-sm"><a href="#">First</a></li>
                        <li><a href="#">Previous</a></li>
                        <li>
                            <ul>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">...</a></li>
                            </ul>
                        </li>
                        <li><a href="#">Next</a></li>
                        <li class="hidden-sm"><a href="#">Last</a></li>
                    </ul>
                </div>
                <div class="wrapper-full settings-panel">
                    <h3 class="no-margin mb-40 text-size-15">Project sections:
                        <i class="sprite-project sprite-project-help help-available"></i>
                    </h3>
                    <div class="row">
                        <div class="input-row">
                            <div class="col-lg-10">
                                <input type="text" class="for-form placeholder-text-italic text-italic-natural text-size-13" placeholder="Type in a section name" />
                            </div>
                            <div class="col-lg-2">
                                <div class="block-center-lg margin-vertical-5-lg">
                                    <div class="row">
                                        <button class="btn panel-darkly-white">+</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsive mb-40">
                    <table border="0" class="table inverse-stripes no-border active-icons text-size-13">
                        <tbody>
                        <tr>
                            <td width="80%">General</td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-edit-row"></i>
                            </td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-delete-row"></i>
                            </td>
                        </tr>
                        <tr>
                            <td width="80%">Main screen</td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-edit-row"></i>
                            </td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-delete-row"></i>
                            </td>
                        </tr>
                        <tr>
                            <td width="80%">Sign up</td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-edit-row"></i>
                            </td>
                            <td class="text-center text-bold">
                                <i class="sprite-project sprite-project-delete-row"></i>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="col-lg-8">
        <div class="content">
            <div class="text-center mb-90">
                <div class="text-border-bt-light mb-80">
                    <h2 class="text-icon no-margin row-hor">Project name <i class="sprite-project sprite-project-edit-gray"></i></h2>
                    <p class="text-icon mb-80 text-size-14">mobile app <i class="sprite-project sprite-project-edit-gray"></i></p>
                </div>
                <h3 class="text-icon text-title-2">Integrations: <i class="sprite-project sprite-project-help"></i></h3>
                <button class="btn btn-primary-blue text-size-13">Add integration</button>
            </div>
            <div class="table-responsive mb-90">
                <table border="0" class="table inverse-stripes no-border active-icons text-size-13">
                    <tbody>
                    <tr>
                        <td><div class="sprite-dialogs sprite-dialogs-git"></div></td>
                        <td width="80%">General</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave" checked><label for="checkboxAutosave"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="sprite-dialogs sprite-dialogs-git"></div></td>
                        <td width="80%">Main screen</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave1" checked><label for="checkboxAutosave1"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    <tr>
                        <td><div class="sprite-dialogs sprite-dialogs-git"></div></td>
                        <td width="80%">Sign up</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave2"><label for="checkboxAutosave2"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="text-center mb-90">
                    <h3 class="text-icon no-margin text-title-2">Bug status scheme: <i class="sprite-project sprite-project-help"></i></h3>
                    <p class="text-icon text-icon-large-indent no-margin text-color-light_brown_2">default status scheme <i class="sprite-project sprite-project-edit-gray"></i></p>
            </div>
            <div class="status-schema-container mb-90 text-size-13">
                    <div class="col-lg-6 col-xs-12">
                        <div class="status-schema-item margin-vertical-5-sm">
                            <div class="toolbar-panel toolbar-panel-small">
                                <div class="toolbar-panel-cell">
                                    <span class="point-rect point-rect-light-red point-rect-circle"></span>
                                </div>
                                <div class="toolbar-panel-cell">
                                    <i class="sprite-project sprite-project-lock"></i>
                                </div>
                                <div class="toolbar-panel-cell">
                                    <span class="text-bold">New</span>
                                </div>
                            </div>
                        </div>
                    </div>

                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-purple point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">Confirmed</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-light-purple point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">Acknowledged</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-pink point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">In progress</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-lime point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">Feedback</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-lime_2 point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">Resolved</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6 col-xs-12">
                    <div class="status-schema-item margin-vertical-5-sm">
                        <div class="toolbar-panel toolbar-panel-small">
                            <div class="toolbar-panel-cell">
                                <span class="point-rect point-rect-light-green_3 point-rect-circle"></span>
                            </div>
                            <div class="toolbar-panel-cell">
                                <i class="sprite-project sprite-project-lock"></i>
                            </div>
                            <div class="toolbar-panel-cell">
                                <span class="text-bold">Closed</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="text-center mb-90">
                <h3 class="text-icon no-margin text-title-2">Bug type scheme: <i class="sprite-project sprite-project-help"></i></h3>
                <p class="text-icon text-color-light_brown_2 text-icon-large-indent no-margin">default status scheme <i class="sprite-project sprite-project-edit-gray"></i></p>
            </div>

        <div class="status-schema-container mb-90 text-bold text-size-13">
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    Functional
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    Usability
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    Text
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    Visual
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    User input data
                </div>
            </div>
            <div class="col-lg-6 col-xs-12">
                <div class="status-schema-item margin-vertical-5-sm">
                    Suggestion
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="text-center mb-90">
             <h3 class="text-icon no-margin text-title-2">Send email notifications: <i class="sprite-project sprite-project-help"></i></h3>
        </div>

                <div class="input-row mb-40">
                    <div class="col-lg-1">
                        <div class="input-row-title text-center text-bold required-label text-size-13">User<sup>*</sup>:</div>
                    </div>
                    <div class="col-lg-10">
                        <div class="auto-complete-row">
                            <input type="text" class="for-form placeholder-text-italic text-italic-natural" placeholder="Type in a section name"
                                   style="padding-left: 187px;">
                            <div class="items-container">
                                <div>Simon CST</div>
                                <div>Simon P</div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-1">
                        <div class="block-center-lg margin-vertical-5-lg">
                            <div class="row">
                                <button class="btn panel-darkly-white">+</button>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            <div class="table-responsive mb-22">
                <table border="0" class="table inverse-stripes no-border small-padding active-icons text-size-13">
                    <tbody>
                    <tr>
                        <td>
                            <div class="avatar-wrapper">
                                <img src="/themes/leantesting/images/sim.jpg" alt="">
                            </div>
                        </td>
                        <td width="80%">General</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave3" checked><label for="checkboxAutosave3"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="avatar-wrapper">
                                <img src="/themes/leantesting/images/sim.jpg" alt="">
                            </div>
                        </td>
                        <td width="80%">Main screen</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave4" checked><label for="checkboxAutosave4"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="avatar-wrapper">
                                <img src="/themes/leantesting/images/sim.jpg" alt="">
                            </div>
                        </td>
                        <td width="80%">Sign up</td>
                        <td>
                            <div class="checkbox-move-wr">
                                <input type="checkbox" class="move" id="checkboxAutosave5"><label for="checkboxAutosave5"></label>
                            </div>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-edit-row"></i>
                        </td>
                        <td class="text-center text-bold">
                            <i class="sprite-project sprite-project-delete-row"></i>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

           <div class="clearfix"></div>
        </div>
    </div>
</div>
</div>