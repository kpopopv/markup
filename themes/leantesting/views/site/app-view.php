<?php

/* @var $this yii\web\View */

$this->title = 'App view';
?>

<section class="text-center">
    <h1 class="text-bold text-title-1">Register a new OAuth application:</h1>
    <img src="/themes/leantesting/images/profile-image.png"/>
</section>
<div class="fields">
    <div class="text-static clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Client ID:</label>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
            <span>Loremipsumxsljhfsuhgwj4oyp9845uigb</span>
        </div>
    </div>
    <div class="text-static clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Client secret:</label>
        </div>
        <div class="col-lg-4 col-md-6 text-center">
            <span>Loremipsumkjdahgp9q8h43ituhzkdhpze98ty</span>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Application name:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Loremipsum"/>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Homepage URL:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="http://loremipsumapp.com"/>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Application description:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="I don't wanna lose you, Leon."/>
        </div>
    </div>
    <div class="clearfix">
        <div class="col-lg-offset-3 col-lg-2 col-md-offset-1 col-md-3 col-sm-12 text-right block-center-sm">
            <label>Authorization callback URL:</label>
        </div>
        <div class="col-lg-4 col-md-6">
            <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="http://loremipsumapp.com/integrations/leantesting"/>
        </div>
    </div>
    <div class="uploader clearfix">
            <div class="col-lg-offset-3 col-lg-2  col-md-4 col-sm-12 text-right block-center-sm">
                <label>Logo<sup>*</sup>:</label>
            </div>
            <div class="col-lg-4 col-md-6">
                        <button type="button" class="btn btn-primary-blue_2 text-size-11">Upload</button>
                        <span class="text-bold text-color-light_brown_2 text-size-11">Recommended&nbsp;size&nbsp;:&nbsp;144x144&nbsp;px</span>
            </div>
    </div>
    <div class="row text-center">
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <button type="button" class="btn btn-primary-blue_2 text-bold text-size-14">Update application</button>
        </div>
        <div class="col-lg-6 col-sm-6 col-xs-12">
            <button type="button" class="btn btn-primary-red text-bold text-size-14">Delete application&nbsp;</button>
        </div>
    </div>
</div>


