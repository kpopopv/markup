<?php

/* @var $this yii\web\View */

$this->title = 'Users view';
?>


<div class="text-center text-size-13">
    <section class="wrapper">
        <h3 class="no-margin text-title-1">Internal users</h3>
    </section>
    <p class="no-margin mb-40 text-color-light_brown_2 wrapper-horizontal adaptive-paragraph">
        Internal users are a part of yor organization and have access to all your projects. This is intended
        to be used by employees of your company that are involved in one or many of your projects.
    </p>
    <p class="no-margin mb-40 text-color-light_brown_2 wrapper-horizontal">
        To add internal users, go to <a href="#" class="link link-blue-1">organization settings</a> page.
    </p>
    <p class="list-title text-bold no-margin mb-40 text-color-light_brown_2">
        Current internal users:
    </p>
</div>

    <ul class="users-list block-center-xs block-center-sm col-lg-12 text-size-13">
        <li class="row">
            <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
                <div class="avatar-wrapper">
                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-2 col-xs-12 text-center separator-item">
                Lorem Ipsum
                <div class="separator hidden-xs pull-right"></div>
            </div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="#" class="link link-blue-1">Project manager</a>
            </div>
        </li>
        <li class="row btn-primary-darkly_white">
            <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
                <div class="avatar-wrapper">
                    <img src="/themes/leantesting/images/sim.jpg" alt="">
                </div>
            </div>
            <div class="col-lg-2 col-xs-12 text-center separator-item">
                Lorem Ipsum
                <div class="separator hidden-xs pull-right"></div>
            </div>
            <div class="col-lg-2 col-xs-12 text-center">
                <a href="#" class="link link-blue-1">Project manager</a>
            </div>
            <div class="clearfix" style="display: block"></div>
        </li>
    </ul>

<div class="clearfix"></div>
<div class="text-center">
    <section class="wrapper">
        <h3 class="no-margin text-title-1">External users</h3>
    </section>
    <p class="no-margin mb-80 text-color-light_brown_2 wrapper-horizontal adaptive-paragraph text-size-13">
        External users only have access to this project and no other. This is intended to be
        used by external collaborators who only required access to this project.
    </p>
    <p class="text-color-violet text-bold no-margin text-size-13">
        Invite external users:
    </p>
    <div class="toolbar-panel toolbar-panel-small wrapper-full">
        <div class="toolbar-panel-cell margin-vertical-5-sm block-center-xs">
            <input type="text" class="for-form placeholder-text-italic text-italic-natural block-center-xs text-size-13" placeholder="Username or e-mail..." />
        </div>
        <div class="toolbar-panel-cell margin-vertical-5-sm block-center-xs">
            <div class="select select-primary block-center-xs">
                <select class="text-size-11 block-center-xs ">
                    <option disabled="" selected="">Select role...</option>
                    <option value="1">Guest</option>
                    <option value="2">Owner</option>
                </select>
            </div>
        </div>
        <div class="toolbar-panel-cell block-center-xs">
            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue block-center-xs">Invite</button>
        </div>
    </div>
    <p class="list-title text-bold no-margin mb-40 text-color-light_brown_2 text-size-13">
        Current internal users:
    </p>
</div>
<ul class="users-list col-xs-12 block-center-sm text-size-13">
    <li class="row">
        <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
            <div class="avatar-wrapper">
                <img src="/themes/leantesting/images/sim.jpg">
            </div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center separator-item">
            Lorem Ipsum
            <div class="separator hidden-xs pull-right"></div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center">
            <a href="#" class="link link-blue-1">Project manager</a>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-delete-row"></i>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-edit-row"></i>
        </div>
    </li>
    <li class="row btn-primary-darkly_white">
        <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
            <div class="avatar-wrapper">
                <img src="/themes/leantesting/images/sim.jpg" alt="">
            </div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center separator-item">
            Lorem Ipsum
            <div class="separator hidden-xs pull-right"></div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center">
            <a href="#" class="link link-blue-1">Project manager</a>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-delete-row"></i>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-edit-row"></i>
        </div>
        <div class="clearfix" style="display: block"></div>
    </li>
</ul>
<div class="text-center">
    <section class="wrapper">
        <h3 class="no-margin text-title-1">Beta testers</h3>
    </section>
    <p class="no-margin mb-80 text-color-light_brown_2 wrapper-horizontal adaptive-paragraph text-size-13">
        Invite fans and friends to test your new releases. This is intended to be used by fans of your company’s
        products who have volunteered to help test your products  before they are officially released.
    </p>
    <p class="text-color-violet text-bold no-margin text-size-13">
        Invite beta testers:
    </p>
    <div class="toolbar-panel toolbar-panel-small wrapper-full">
        <div class="toolbar-panel-cell margin-vertical-5-sm block-center-xs">
            <input type="text" class="for-form placeholder-text-italic text-italic-natural block-center-xs invite-beta-testers text-size-13" placeholder="E-mail address..." />
        </div>
        <div class="toolbar-panel-cell block-center-xs">
            <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue block-center-xs">Invite</button>
        </div>
    </div>
    <p class="list-title text-bold no-margin mb-40 text-size-13">
        Current internal users:
    </p>
</div>
<div class="row wrapper-full">
    <div class="col-lg-6 col-sm-12 text-center">
        <div class="recruit-title toolbar-panel">
            <i class="sprite-project sprite-project-user toolbar-panel-cell"></i>
            <h3 class="toolbar-panel-cell text-size-15">Recruit beta testers</h3>
        </div>
        <p class="text-color-light_brown_2 text-size-13">
            Copy & paste this code onto your website and start recruiting<br/>
            beta testers for future releases of your products.
        </p>

    </div>
    <div class="col-lg-6 col-sm-12">
        <div class="panel-light-blue_2 code-panel">
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
            &lt;form id='contact_form' action='#' method='POST' enctype='multipart/form-data'&gt;
        </div>
    </div>
</div>
<p class="list-title text-bold no-margin mb-40 text-center text-color-light_brown_2 text-size-13">
    Current beta testers:
</p>
<ul class="users-list block-center-xs block-center-sm text-size-13">
    <li class="row">
        <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
            <div class="avatar-wrapper">
                <img src="/themes/leantesting/images/sim.jpg" alt="">
            </div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center separator-item">
            Lorem Ipsum
            <div class="separator hidden-xs pull-right"></div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center">
            <a href="#" class="link link-blue-1">Project manager</a>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-delete-row"></i>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-edit-row"></i>
        </div>
    </li>
    <li class="row btn-primary-darkly_white">
        <div class="col-lg-1 col-xs-12 text-center block-center-xs ">
            <div class="avatar-wrapper">
                <img src="/themes/leantesting/images/sim.jpg" alt="">
            </div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center separator-item">
            Lorem Ipsum
            <div class="separator hidden-xs pull-right"></div>
        </div>
        <div class="col-lg-2 col-xs-12 text-center">
            <a href="#" class="link link-blue-1">Project manager</a>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-delete-row"></i>
        </div>
        <div class="col-lg-1 col-xs-6 text-center pull-right-md">
            <i class="sprite-project sprite-project-edit-row"></i>
        </div>
        <div class="clearfix" style="display: block"></div>
    </li>
</ul>
<div class="text-center">
    <section class="wrapper">
        <h3 class="no-margin text-title-1">Professional testers</h3>
    </section>
    <p class="list-title no-margin mb-80 text-color-light_brown_2 wrapper-horizontal adaptive-paragraph text-size-13">
        If you need help testing your interactive products, we have professional testers available. Whether you are looking for
        steady, dedicated resources or sporadic testing help from testers in various parts of the world, we can help.
    </p>
    <section class="text-center panel-light-blue_2 mb-80">
        <button type="button" class="btn btn-primary-blue text-bold text-size-15 text-title-4">Hire professional testers now</button>
    </section>
    <p class="text-bold text-size-13">Who are professional testers?</p>
    <p class="mb-80 text-color-light_brown_2 wrapper-horizontal adaptive-paragraph text-size-13">Professional testers are sourced from our curated community of freelance testers, <b>Crowd Testing</b>, or internal,
        full-time employyes of our parent company <b>QA on Request</b> based in Montreal, Canada.</p>
</div>
<div class="clearfix"></div>


