<?php

/* @var $this yii\web\View */

$this->title = 'Account settings 3';
?>

<div class="wrapper text-center">
  <h3 class="mb-40 text-title-2">Change your password:</h3>
    <div class="fields account-password-fields mb-36 text-size-13">
        <div class="row mb-12">
            <div class="col-lg-12 col-md-12">
                <label class="required-label text-bold">Current password<sup>*</sup>:</label>
            </div>
            <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Type in your current password">
            </div>
        </div>
        <div class="row mb-12">
            <div class="col-lg-12 col-md-12">
                <label class="required-label text-bold">New password<sup>*</sup>:</label>
            </div>
            <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Minimum 6 characters">
            </div>
        </div>
        <div class="row mb-28">
            <div class="col-lg-12 col-md-12">
                <label class="required-label text-bold">Confirm new password<sup>*</sup>:</label>
            </div>
            <div class="col-lg-offset-3 col-lg-6 col-md-offset-3 col-md-6">
                <input class="for-form text-center placeholder-text-italic text-italic-natural" placeholder="Confirm your new password">
            </div>
        </div>
        <div class="text-center toolbar-panel toolbar-panel-small pg-top-20">
            <div class="toolbar-panel-cell">
                <button type="button" class="btn btn-primary-darkly_white">Cancel</button>
            </div>
            <div class="toolbar-panel-cell">
                <button type="button" class="btn btn-primary-blue_2">Update password</button>
            </div>
        </div>
    </div>
</div>
