<?php

/* @var $this yii\web\View */

$this->title = 'Sign up';
?>

<main class="box col-xs-12">
    <div class="box-header">
        <div class="text-bold text-center box-title">Create your Leantesting account</div>
        <p class="text-bold text-center box-sub-title">100% free, no credit card required</p>
    </div>
    <div class="box-content">
        <input name="email" class="for-form center-placeholder text-center" placeholder="E-mail address*"/>
        <input name="username" class="for-form center-placeholder text-center" placeholder="Username*"/>
        <p class="text-bold text-color-light_brown_2 info">
            Up to 15 characters (letters, digits and hyphens only).
        </p>
        <input name="password" class="for-form center-placeholder text-center" placeholder="Password*"/>
        <div data-before="http://" data-after=".leantesting.com" class="organization-group">
            <div class="organization-input-wrapper">
                <input name="Organization" class="for-form center-placeholder text-center" placeholder="Organization"/>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
    <div class="box-footer">
        <button type="button" class="btn btn-primary-blue btn-lg-w-100 btn-lg-h">Create account</button>
        <button type="button" class="btn btn-primary-darkly_white btn-lg-w-100 btn-lg-h text-color-light_brown_2">
            <div class="sprite-dialogs sprite-dialogs-git"></div>
            or sign up with GitHub
        </button>
    </div>
</main>
<div class="clearfix"></div>
<p class="useful_links text-center">Already with us? <a href="#">Sign in</a></p>