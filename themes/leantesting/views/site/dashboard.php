<div class="text-center">
    <h1 class="text-title-16 ">Welcome to Lean Testing!</h1>
    <i class="sprite-dashboard sprite-dashboard-logo"></i>
    <div class="block-welcome text-left text-semibold">
        <p>If you manage projects, the first thing you need to do is  <a href="#" class="link link-blue-1 link-bg-light-blue">&nbsp;create&nbsp;your&nbsp;organization&nbsp;</a>&nbsp;.</br>
            You’ll be able to create projects, manage bug reports, create and execture test plans and invite your colleagues.
        </p>
        <p>
            If you don’t manage projects and you see this message, it means that someone needs to invite you to their project.</br>
            Talk to your colleagues and ask them to invite you to the project using your username or e-mail address.
        </p>
        <p>
            While you’re waiting, feel free to join our  <a href="#" class="link link-blue-1 link-bg-light-blue"> community </a>  and meet other fellow Lean Testing users.
        </p>
        <p>
            If you have questions about getting started, visit our <a href="#" class="link link-blue-1 link-bg-light-blue"> FAQs&nbsp;</a>&nbsp;.
        </p>
        <p>
            Thank you for signing up!</br>
            Please don’t hesitate to get in touch and share your feedback, good or bad.
        </p>
    </div>
</div>

