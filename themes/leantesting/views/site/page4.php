<?php

/* @var $this yii\web\View */

$this->title = 'Test plan edit';
?>

<div class="block block-top row">
    <div class="wrapper">
        <header class="text-title-2 text-bold">
            Test plan: <span class="text-color-light_brown_2">“</span>QAOR Redesign<span class="text-color-light_brown_2">”</span>
        </header>

        <div class="text-color-light_brown_2 text-title-3">
            Sample test plan name
            <div class="hidden-lg hidden-md hidden-sm"></div>
            <span>Version:</span>
            <span class="text-color-blue text-bold">1.4.4</span>
        </div>

        <div class="group-select">
            <div class="group-select_title">
                <span class="text-color-blue">*</span><span class="text-italic text-color-light_brown_2 text-title-4">Optional - select a defeault device for bug reports:</span>
            </div>

            <div class="group-select_body">
                <div class="select select-primary">
                    <select>
                        <option disabled selected>Device type</option>
                        <option value="1">Mobile</option>
                        <option value="2">Table</option>
                        <option value="3">Laptop</option>
                    </select>
                </div>

                <div class="select select-primary">
                    <select>
                        <option disabled selected>Device family</option>
                        <option value="1">Mobile</option>
                        <option value="2">Table</option>
                        <option value="3">Laptop</option>
                        <option value="4">Desktop</option>
                    </select>
                </div>

                <div class="select select-primary">
                    <select>
                        <option disabled selected>Device OS</option>
                        <option value="1">Ios</option>
                        <option value="2">Android</option>
                        <option value="3">Windows</option>
                    </select>
                </div>

                <div class="select select-primary">
                    <select>
                        <option disabled selected>Browser</option>
                        <option value="1">Google Chrome</option>
                        <option value="2">Firefox</option>
                        <option value="3">Safary</option>
                        <option value="4">Opera</option>
                    </select>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="block block-top row">
    <div class="wrapper">
            <div class="toolbar-panel toolbar-panel-height-27">
                <div class="toolbar-panel-cell block-center-xs float-none-xs">
                    <input type="checkbox" id="test1" />
                    <label for="test1" class="text-color-light_brown_2">Select all</label>
                    &nbsp;&nbsp;
                    <button type="button" class="btn btn-primary-darkly_white">Edit selected</button>
                </div>
                <div class="toolbar-panel-cell no-margin-right pull-right block-center-xs float-none-xs">
                    <a href="#" class="link link-light_brown_2">Expand</a> / <a href="#" class="link link-light_brown_2">collapse all</a>
                </div>
            </div>
    </div>
</div>

<div class="wrapper">
    <div class="row">
        <div class=col-md-12>

            <!-- Component #1 -->
            <div class="panel-group" role="tablist" aria-multiselectable="true">
                <div class="panel-heading" role="tab"">
                    <div class="panel-title">
                        <div class="panel-darkly-brown">
                            <input type="checkbox" id="test2"><label for="test2"></label>
                            <a role="button"
                               data-toggle="collapse"
                               data-parent="#accordion"
                               href="#collapseRootOne"
                               aria-expanded="true"
                               class="text-bold pg-16 link link-white">Component #1:</a>
                            <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>
                        </div>
                    </div>
                </div>

                <div id="collapseRootOne" class="panel-collapse collapse in" role="tabpanel">
                    <div class="panel-body">

                        <!-- Case #1 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-child">
                                        <input type="checkbox" id="test4"><label for="test4"></label>
                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildOne"
                                           aria-expanded="false"
                                           class="text-bold text-color-violet pg-16 link">Case #1:</a>
                                        <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                        <ul class="panel-menu">
                                            <li>
                                                <div class="indicator" data-n="3">
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="btn-group">
                                                    <button type="button" class="dropdown-toggle no-select btn btn-fix-w-114 btn-primary-light-green" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Pass</button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0);" data-toggle-text="Pass" data-toggle-class="light-green">Pass</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="Fail" data-toggle-class="red">Fail</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="CNT" data-toggle-class="light-brown">Could not test</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="N/A" data-toggle-class="grey">Not applicable</a></li>
                                                    </ul>
                                                </div>
                                            </li>

                                            <li>
                                                <button type="button" class="btn btn-fix-w-114 btn-primary-blue">Report bug</button>
                                            </li>

                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildOne"
                                                   class="sprite sprite-collapse-grey"
                                                   aria-expanded="false">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseChildOne" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <div class="row">
                                        <div class="col-lg-4 mb-22">
                                            <section>
                                                <header class="text-bold">Steps to reproduce:</header>

                                                <div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">1.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">2.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">3.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">4.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">5.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                </div>

                                            </section>
                                        </div>
                                        <div class="col-lg-4 mb-22">
                                            <div class="row">
                                                <div class="col-md-12 mb-22">
                                                    <section>
                                                        <header class="text-bold">Preconditions:</header>
                                                        <div class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing
                                                            elit. Praesent vitae neque nec elit bibendum
                                                            tristique. Sed iaculis scelerisque eros, eget
                                                            venenatis orci mollis quis. Cras vestibulum augue.
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="col-md-12">
                                                    <section>
                                                        <header class="text-bold">Expected result:</header>
                                                        <div class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing
                                                            elit. Praesent vitae neque nec elit bibendum
                                                            tristique. Sed iaculis scelerisque eros, eget
                                                            venenatis orci mollis quis. Cras vestibulum augue.
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <section>
                                                <header class="text-bold">Reference image:</header>

                                                <div class="row">
                                                    <div class="col-md-12 col-lg-6 col-sm-6 mb-22">
                                                        <div class="attachment"></div>
                                                    </div>
                                                    <div class="col-md-12 col-lg-6 col-sm-6">
                                                        <div class="drag-and-drop">
                                                            <div class="sprite sprite-image"></div>
                                                            <div class="drag-and-drop_text">
                                                                <div>Drag and drop a file</div>
                                                                here to attach a
                                                                screenshot or a video.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Case #1 -->

                        <!-- Case #2 -->
                        <div class="panel-group" role="tablist" aria-multiselectable="true">
                            <div class="panel-heading" role="tab"">
                                <div class="panel-title">
                                    <div class="panel-darkly-white panel-child">
                                        <input type="checkbox" id="test5"><label for="test5"></label>
                                        <a role="button"
                                           data-toggle="collapse"
                                           data-parent="#accordion"
                                           href="#collapseChildTwo"
                                           aria-expanded="false"
                                           class="text-bold text-color-violet pg-16 link">Case #2:</a>
                                        <span class="text-italic">Lorem ipsum dolor sit amet, consectetur adipiscing elit...</span>

                                        <ul class="panel-menu">
                                            <li>
                                                <div class="indicator" data-n="1">
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                    <div class="indicator_scale"></div>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="btn-group">
                                                    <button type="button" class="dropdown-toggle no-select btn btn-fix-w-114 btn-primary-red" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Fail</button>
                                                    <ul class="dropdown-menu">
                                                        <li><a href="javascript:void(0);" data-toggle-text="Pass" data-toggle-class="light-green">Pass</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="Fail" data-toggle-class="red">Fail</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="CNT" data-toggle-class="light-brown">Could not test</a></li>
                                                        <li><a href="javascript:void(0);" data-toggle-text="N/A" data-toggle-class="grey">Not applicable</a></li>
                                                    </ul>
                                                </div>
                                            </li>

                                            <li>
                                                <button type="button" class="btn btn-fix-w-114 btn-primary-blue">Report bug</button>
                                            </li>

                                            <li>
                                                <a role="button"
                                                   data-toggle="collapse"
                                                   data-parent="#accordion"
                                                   href="#collapseChildTwo"
                                                   class="sprite sprite-collapse-grey"
                                                   aria-expanded="false">
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div id="collapseChildTwo" class="panel-collapse collapse" role="tabpanel">
                                <div class="panel-body panel-body_content">
                                    <div class="row">
                                        <div class="col-lg-4 mb-22">
                                            <section>
                                                <header class="text-bold">Steps to reproduce:</header>

                                                <div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">1.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">2.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">3.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">4.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                    <div class="panel-light-brown panel-sm">
                                                        <span class="text-bold pg-16">5.</span><span class="">Lorem impsum dolor...</span>
                                                    </div>
                                                </div>

                                            </section>
                                        </div>
                                        <div class="col-lg-4 mb-22">
                                            <div class="row">
                                                <div class="col-md-12 mb-22">
                                                    <section>
                                                        <header class="text-bold">Preconditions:</header>
                                                        <div class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing
                                                            elit. Praesent vitae neque nec elit bibendum
                                                            tristique. Sed iaculis scelerisque eros, eget
                                                            venenatis orci mollis quis. Cras vestibulum augue.
                                                        </div>
                                                    </section>
                                                </div>
                                                <div class="col-md-12">
                                                    <section>
                                                        <header class="text-bold">Expected result:</header>
                                                        <div class="text-description">
                                                            Lorem ipsum dolor sit amet, consectetur adipiscing
                                                            elit. Praesent vitae neque nec elit bibendum
                                                            tristique. Sed iaculis scelerisque eros, eget
                                                            venenatis orci mollis quis. Cras vestibulum augue.
                                                        </div>
                                                    </section>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <section>
                                                <header class="text-bold">Reference image:</header>

                                                <div class="row">
                                                    <div class="col-md-12 col-lg-6 col-sm-6 mb-22">
                                                        <div class="attachment"></div>
                                                    </div>
                                                    <div class="col-md-12 col-lg-6 col-sm-6">
                                                        <div class="drag-and-drop">
                                                            <div class="sprite sprite-image"></div>
                                                            <div class="drag-and-drop_text">
                                                                <div>Drag and drop a file</div>
                                                                here to attach a
                                                                screenshot or a video.
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </section>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- End Case #2 -->

                    </div>
                </div>
            </div>
            <!-- End component #1 -->
        </div>
    </div>
</div>

<div class="wrapper" style="margin-top: 16px;">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <div class="col-xs-6">
                <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-darkly_white">Cancel</button>
            </div>
            <div class="col-xs-6">
                <button type="button" class="btn btn-lg btn-lg-w-100 btn-primary-blue">Save & Start</button>
            </div>
        </div>
    </div>
</div>