<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\themes\leantesting\assets\LeanTesting;

LeanTesting::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <?php $this->head() ?>
</head>

<body class="<?php echo isset($this->params['body_layout']) ? $this->params['body_layout'] : '' ?>">
<?php $this->beginBody() ?>
<?= $this->render('_header.php'); ?>

<?php if(Yii::$app->controller->action->id == 'project6'): ?>
    <?php
     Yii::$app->view->registerJs('var page_id = "'.Yii::$app->controller->action->id.'"',  \yii\web\View::POS_HEAD);
     echo $content
    ?>
<?php else: ?>
<!--col-md-9 col-md-offset-2 col-sm-offset-3 col-xs-offset-0-->
<div style="overflow-x: hidden">
    <div class="col-lg-9 col-lg-offset-2 col-md-9 col-md-offset-2 col-sm-9 col-sm-offset-2">
    <?php if(in_array(Yii::$app->controller->action->id,['app-reg', 'app-view'])): ?>
        <?= $this->render('_app-reg-toolbar.php'); ?>
    <?php elseif(Yii::$app->controller->action->id == 'user-dashboard-project'): ?>
        <?= $this->render('_dashboard-project-toolbar.php'); ?>
    <?php elseif(Yii::$app->controller->action->id == 'project1'): ?>
        <?= $this->render('_project1.php'); ?>
    <?php endif; ?>

    <?php if(in_array(Yii::$app->controller->action->id,['project5', 'account', 'account1','account2', 'account3'])): ?>
        <?php if(in_array(Yii::$app->controller->action->id, ['account','account1','account2', 'account3'])): ?>
            <div class="row">
                <div class="col-lg-3">
                    <?= $this->render('_account-sidebar.php'); ?>
                </div>
                <div class="col-lg-<?php echo Yii::$app->controller->action->id == 'account' ? '9' : '8'; ?>">
                    <div class="content">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        <?php else: ?>
        <?= $content ?>
        <?php endif; ?>
    <?php else: ?>
        <article class="content <?= Yii::$app->controller->action->id; ?>">
            <?= $content ?>
        </article>
    <?php endif; ?>
</div>
</div>
<?php endif; ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
