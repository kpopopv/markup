<div class="content pg-0">
    <sidebar>
                    <ul>
                        <li>
                            <b>Account settings:</b>
                            <ul>
                                <li><a href="javascript:void(0)">Profile</a></li>
                                <li><a href="javascript:void(0)">E-mail notifications</a></li>
                                <li><a href="javascript:void(0)">Change password</a></li>
                            </ul>
                        </li>
                        <li>
                            <b>Crowdsourced testing:</b>
                            <ul>
                                <li class="active"><a href="javascript:void(0)">Organization settings</a></li>
                            </ul>
                        </li>
                        <li>
                            <b>Software testing world cup:</b>
                            <ul>
                                <li><a href="javascript:void(0)">Organization information</a></li>
                            </ul>
                        </li>
                        <li>
                            <b>Software testing world cup:</b>
                            <ul>
                                <li><a href="javascript:void(0)">Organization information</a></li>
                            </ul>
                        </li>
                    </ul>
    </sidebar>
</div>
<div class="account-action-panel">
    <div class="wrapper-full">
                <a href="javascript:void(0)" class="link text-color-violet">
                    <div class="toolbar-panel toolbar-panel-small no-margin-top account-action-panel-row">
                        <div class="toolbar-panel-cell">
                            <i class="sprite-account sprite-account-action-plus"></i>
                        </div>
                        <div class="toolbar-panel-cell">
                             <h3 class="no-margin ">Create an organization</h3>
                        </div>
                    </div>
                </a>
        </div>
</div>
