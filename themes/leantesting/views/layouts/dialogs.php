<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\themes\leantesting\assets\LeanTesting;

LeanTesting::register($this);

?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <link href='http://fonts.googleapis.com/css?family=Lato&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <?php $this->head() ?>
</head>
<body class="dialogs-layout">
<?php $this->beginBody() ?>

<header class="text-center">
    <div class="sprite-dialogs sprite-dialogs-logo"></div>
</header>
<?= $content ?>
<footer class="text-center">
    <ul>
        <li><a href="#">Terms and conditions</a></li>
        <li><a href="#">Privacy policy</a></li>
    </ul>
</footer>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
