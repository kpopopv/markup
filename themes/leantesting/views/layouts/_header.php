<header class="header">

    <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-navbar-collapse" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <div class="sprite sprite-logo" style="float: left; margin-top: -10px; margin-left: -10px"></div>
                    <span class="text-bold left" style="float: left">Leantesting</span>
                </a>
            </div>

            <div class="collapse navbar-collapse" id="bs-navbar-collapse">
                <ul class="nav navbar-nav hidden-sm hidden-md">
                    <li>
                        <ul class="breadcrumb">
                            <li>
                                <a href="#">My projects</a>
                            </li>
                            <li>
                                <a href="#">QAOR Redesign</a>
                            </li>
                        </ul>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right hidden-md hidden-sm hidden-lg visible-xs">
                    <li>
                        <a href="#"></a>
                    </li>
                    <li>
                        <a href="#">Bag</a>
                    </li>
                    <li>
                        <a href="#">Test</a>
                    </li>
                    <li>
                        <a href="#">Chat</a>
                    </li>
                    <li>
                        <a href="#">Sdk</a>
                    </li>
                    <li>
                        <a href="#">Users</a>
                    </li>
                    <li>
                        <a href="#">Setting</a>
                    </li>
                    <li>
                        <a href="#">Apps</a>
                    </li>
                    <li>
                        <a href="#">
                            <span>Hello,</span>&nbsp;<span class="text-bold">Pawel Utr</span>
                        </a>
                    </li>
                </ul>

                <ul class="nav navbar-nav navbar-right hidden-xs">
                    <li>
                        <a href="#">
                            <div class="sprite sprite-apps"></div>
                        </a>
                    </li>
                    <li class="separator"></li>
                    <li class="wr">
                        <a href="#">
                            <span>Hello,</span>&nbsp;<span class="text-bold">Pawel Utr</span>
                        </a>
                    </li>
                    <li>
                        <div class="btn-group">
                            <a href="#" class="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" data-toggle-replace="false">
                                <span class="sprite sprite-user"></span>
                                <span class="arrow-down"></span>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Profile</a></li>
                                <li><a href="#">Setting</a></li>
                                <li><a href="#">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>

        </div>
    </nav>

    <nav class="left-navbar hidden-xs">
        <ul class="left-navbar-nav initialized">
            <li class="item-menu">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-dash"></div>
                    <span class="text-size-11">Dashboard</span>
                </a>
            </li>
            <li class="item-menu">
                <div style="position: relative">
                    <a href="javascript:void(0);">
                        <div class="sprite-sidebar sprite-sidebar-bug"></div>
                        <span class="text-size-11">Bug tracker</span>
                    </a>
                    <div id="btn-show-bug-tracker">
                       <i class="sprite-project sprite-project-collapse"></i>
                    </div>
                </div>
            </li>
            <li class="item-menu active">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-test"></div>
                    <span class="text-size-11">Test cases</span>
                </a>
            </li>
            <li class="item-menu">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-chat"></div>
                    <span class="text-size-11">Files&nbsp;&&nbsp; Conversations</span>
                </a>
            </li>
            <li class="item-menu">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-sdk"></div>
                    <span class="text-size-11">SDK</span>
                </a>
            </li>
            <li class="item-menu">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-users"></div>
                    <span class="text-size-11">Users</span>
                </a>
            </li>
            <li class="item-menu">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-settings"></div>
                    <span class="text-size-11">Project settings</span>
                </a>
            </li>
            <li class="item-menu no-border-bottom" id="btn-collapse-navbar">
                <a href="javascript:void(0);">
                    <div class="sprite-sidebar sprite-sidebar-expand"></div>
                    <span class="text-size-11">Collapse sidebar</span>
                </a>
            </li>
            <li class="no-border-bottom bg">
                <a href="javascript:void(0);" class="link link-blue">Quick access</a>
                <a href="javascript:void(0);" class="link link-blue">FAQ & help</a>
            </li>
        </ul>
    </nav>

</header>