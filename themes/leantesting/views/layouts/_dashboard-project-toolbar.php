<div class="toolbar-panel">
    <div class="col-lg-10 col-sm-12">
        <div class="row">
                <div class="toolbar-panel-cell block-center-xs text-size-13">
                    <label for="jump-to-project text-bold toolbar-panel-cell">Jump to a project:</label>
                </div>
                <div class="toolbar-panel-cell block-center-xs">
                    <div class="select select-primary select-border-darkly">
                        <select id="jump-to-project">
                            <option>List of projects in alphabetical order</option>
                        </select>
                    </div>
                </div>
                <div class="toolbar-panel-cell block-center-xs text-size-13">
                    <a class="link link-blue-1" href="#">Go to your archived projects</a>
                </div>
        </div>
    </div>
    <div class="col-lg-2 col-sm-12 row-xs text-right">
        <div class="panel-group block-center-xs" role="tablist" aria-multiselectable="true">
            <div class="panel-heading block-center-xs row toolbar-panel-cell" role="tab" "="">
                <div class="panel-title">
                    <button role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseRootOne" aria-expanded="false" class="block-center-xs panel-select-header text-bold pg-16 link link-white collapsed">Bugs assigned to me
                        <i class="sprite-dashboard sprite-dashboard-arrow"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="clearfix"></div>

<div id="collapseRootOne" class="panel-select-body panel-collapse collapse out" role="tabpanel" aria-expanded="true">
    <div class="content">
        <div class="col-lg-12 col-md-12 mb-12">
            <div class="row">
                <div class="wrapper-small">
                    <h3>The following bugs are assigned to you:</h3>
                    <p class="text-size-11">Project No 1:</p>
                </div>
                <div class="table-responsive">
                    <table class="table no-border inverse-stripes cells-border-separator">
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#100</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#101</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#102</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                    </table>
                </div>
                <div class="wrapper-small">
                    <p class="text-size-11">Project No 2:</p>
                </div>
                <div class="table-responsive">
                    <table class="table no-border inverse-stripes cells-border-separator text-size-11">
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#100</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#101</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                        <tr>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">#102</a></div></td>
                            <td><div class="cell-content"><a href="#" class="link link-blue-1">Can’t submit stories and a really really really long title...</a></div></td>
                            <td><div class="cell-content">Component name</div></td>
                            <td><div class="cell-content">Version 1.0.1</div></td>
                            <td><div class="cell-content">Status</div></td>
                            <td><div class="cell-content">Priority</div></td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
</div>