<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\themes\leantesting\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class LeanTesting extends AssetBundle
{
    public $basePath = '@webroot';
    public $jsOptions = array(
        'position' => \yii\web\View::POS_HEAD
    );
    public $css = [
        'themes/leantesting/style/styles.css',
        'themes/leantesting/javascript/stacktable.css'
    ];
    public $js = [
        'themes/leantesting/javascript/default.js',
        'themes/leantesting/javascript/bug-tracker.js',
        'themes/leantesting/javascript/stacktable.min.js'
    ];
    public $depends = [
        'app\themes\leantesting\assets\Jquery',
        'app\themes\leantesting\assets\Bootstrap',
    ];
}
